# GUESS!

GUESS! is a fast word-guessing game. In GUESS!, players compete to guess categorized words faster than their opponents.
The players form words using letters provided, earning points based on speed.

*Step-by-step instruction on how to install, compile and run the game:*

1. Clone project from Gitlab
2. Open the project in Android Studio
3. Go to branch main
4. Sync Project with Gradle Files from File in menu
5. Use Android as configuration
6. Run using android phone
7. View the Android Device from Device manager
8. The game will start


*Step-by-step on how to play the game:*

1. Access «How to play» for instructions on the game.
2. Turn on/off music and sound using the setting button.
2. Create a new game by clicking «New Game», you then become the host and are given a PIN after creating the game.
3. Join game by selecting «Join game» and entering the PIN you receive from the host.
4. Hosts initiate the game when all initial players are in the lobby.
5. Players wait for the host to start the game in the lobby.
6. Click on letters to guess the word within the given category and word length.
7.  Earn points based on the speed of correct guesses.
8. View the leaderboard between rounds to compare scores.
9. Hosts click on "Next Round" to start the new round, while players await the host to begin.
10. After all rounds, view the final leaderboard to see overall scores.

** Hint **

EXCUTING GAME ROUND FOR TESTING PURPOSES: SEE LOG (LOGCAT) FOR WORD BEING PLAYED TO GUESS CORRECTLY


*Description of our structure:*

Android module contains:
- *android/java/com.wordbattle.game* contains AndroidInterfaceClass and AndroidLauncher, they launch the application
- *android/assets* contains all our styling-objects

Core module contains
- **core/main/java/com.wordbattle.game/controller** with all the controllers
- **core/main/java/com.wordbattle.game/manager** contains MusicManager and SoundManager
- **core/main/java/com.wordbattle.game/model** with all the models
- **core/main/java/com.wordbattle.game/network** contains FirebaseInterface and FirebaseManager
- **core/main/java/com.wordbattle.game/states** with alle the states
- **core/main/java/com.wordbattle.game/view** with alle the views
- **core/main/java/com.wordbattle.game** contains WordBattle
