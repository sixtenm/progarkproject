package com.wordbattle.game;

import android.os.Bundle;
import android.util.Log;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.firebase.FirebaseApp;
import com.wordbattle.game.WordBattle;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("AndroidLauncher", "Database is initialized"); // Add this line for logging
		FirebaseApp.initializeApp(this);


		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new WordBattle(new AndroidInterfaceClass()), config);
	}
}
