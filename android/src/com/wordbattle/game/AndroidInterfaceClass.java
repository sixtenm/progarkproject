package com.wordbattle.game;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wordbattle.game.model.Category;
import com.wordbattle.game.model.LobbyModel;
import com.wordbattle.game.model.PlayerModel;
import com.wordbattle.game.model.Word;
import com.wordbattle.game.network.FirebaseInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class AndroidInterfaceClass implements FirebaseInterface {

    FirebaseDatabase database;
    DatabaseReference myRef;


    public AndroidInterfaceClass() {
        database = FirebaseDatabase.getInstance("https://wordbattle-96156-default-rtdb.europe-west1.firebasedatabase.app");
        myRef = database.getReference("message");
        myRef.setValue("Something");
    }

    @Override
    public void SomeFunction() {
        System.out.println("Something works");
    }

    @Override
    public void createNewLobby(String hostNickname, String pin, String numRounds) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        Map<String, Object> lobbyData = new HashMap<>();
        lobbyData.put("gameStarted", false);
        lobbyData.put("hostNickname", hostNickname);
        lobbyData.put("pin", pin);
        lobbyData.put("NumberOfRounds", numRounds);

        // Create a map to store players' scores and finished status
        Map<String, Object> players = new HashMap<>();
        // Add the host player
        players.put(hostNickname, new HashMap<String, Object>() {{
            put("score", 0);
            put("finished", false);
        }});
        // Add the players to the lobby data
        lobbyData.put("players", players);

        lobbyRef.setValue(lobbyData)
                .addOnSuccessListener(aVoid -> {
                    // This will be called if the lobby is successfully created in the database.
                    // Here you could navigate to the lobby screen, update the UI, etc.
                    System.out.println("Lobby created successfully.");
                })
                .addOnFailureListener(e -> {
                    // This will be called if there is an error creating the lobby.
                    // Here you could display an error message or perform some error handling.
                    System.err.println("Error creating lobby: " + e.getMessage());
                });
    }

    @Override
    public void joinLobby(String pin, String playerNickname, Runnable onSuccess, Runnable onFail) {
        // Reference to the specific lobby by its pin
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);

        // Get the current state of the lobby to add a new player
        lobbyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> lobbyData = (Map<String, Object>) dataSnapshot.getValue();
                if (lobbyData != null) {
                    // Get the players map from the lobby data
                    Map<String, Object> players = (Map<String, Object>) lobbyData.get("players");
                    if (players != null) {
                        // Add the new player to the players map
                        players.put(playerNickname, new HashMap<String, Object>() {{
                            put("score", 0);
                            put("finished", false);
                        }});
                        // Update the players map in the lobby data
                        lobbyData.put("players", players);

                        // Save the updated lobby back to the database
                        lobbyRef.setValue(lobbyData).addOnSuccessListener(aVoid -> {
                            // Successfully joined the lobby
                            System.out.println(playerNickname + " joined the lobby with PIN: " + pin);
                            onSuccess.run();
                        }).addOnFailureListener(e -> {
                            // Handle failure to join the lobby
                            System.err.println("Failed to join lobby: " + e.getMessage());
                        });
                    } else {
                        // Players map is null, something went wrong
                        System.err.println("Error: Players map is null in lobby " + pin);
                        onFail.run();
                    }
                } else {
                    // Lobby data is null, lobby does not exist
                    System.err.println("Error: Lobby with PIN " + pin + " does not exist.");
                    onFail.run();
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle cancellation
                System.err.println("Error joining lobby: " + databaseError.getMessage());
                onFail.run();
            }
        });
    };


    public void createNewCategory(Category category) {
            DatabaseReference categoryRef = database.getReference("categories").child(category.getName());
            categoryRef.setValue(category)
                    .addOnSuccessListener(aVoid -> {
                        System.out.println("Category " + category.getName() + " created successfully.");
                    })
                    .addOnFailureListener(e -> {
                        System.err.println("Error creating category " + category.getName() + ": " + e.getMessage());
                    });
    }

    @Override
    public void addWordToCategory(String categoryName, Word word) {
        DatabaseReference wordsRef = database.getReference("categories").child(categoryName).child("words");
        wordsRef.child(word.getWord()).setValue(word)
                .addOnSuccessListener(aVoid -> {
                    // Handle success
                    System.out.println("Word " + word.getWord() + " added to category " + categoryName + " successfully.");
                })
                .addOnFailureListener(e -> {
                    // Handle failure
                    System.err.println("Error adding word " + word.getWord() + " to category " + categoryName + ": " + e.getMessage());
                });
    }

    @Override
    public void addScoreToDatabase(String pin, String nickname) {
        DatabaseReference scoreRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(pin).child("players").child(nickname).child("score");
        System.out.println("Inside addInitialScore");

        // Initialize score to 0 for the nickname
        scoreRef.setValue(0)
                .addOnSuccessListener(aVoid -> {
                    System.out.println("Score for " + nickname + " initialized to 0.");
                })
                .addOnFailureListener(e -> {
                    System.err.println("Failed to add score to database for " + nickname + ": " + e.getMessage());
                });
    }



    @Override
    public void updateScoreInDatabase(String pin, String nickname, int scoreToAdd, ScoreUpdateCallback callback) {
        DatabaseReference scoreRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(pin).child("players").child(nickname).child("score");

        // First, read the current score
        scoreRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Integer currentScore = dataSnapshot.getValue(Integer.class);
                if (currentScore == null) {
                    currentScore = 0; // Default to 0 if not set
                }
                int newScore = currentScore + scoreToAdd;

                // Now, update the score with the new value
                scoreRef.setValue(newScore)
                        .addOnSuccessListener(aVoid -> {
                            System.out.println("Score for " + nickname + " updated successfully. New score: " + newScore);
                            if (callback != null) {
                                callback.onSuccess();
                            }
                        })
                        .addOnFailureListener(e -> {
                            System.err.println("Failed to update score for " + nickname + ": " + e.getMessage());
                            if (callback != null) {
                                callback.onFailure();
                            }
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.err.println("Failed to read current score for " + nickname + ": " + databaseError.getMessage());
                if (callback != null) {
                    callback.onFailure();
                }
            }
        });
    }




    @Override
    public void fetchScores(String lobbyPin, ScoreFetchCallback callback) {
        DatabaseReference scoresRef = FirebaseDatabase.getInstance().getReference("lobbies").child(lobbyPin).child("scores");

        scoresRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Map<String, Integer> scores = new HashMap<>();
                for (DataSnapshot scoreSnapshot : snapshot.getChildren()) {
                    scores.put(scoreSnapshot.getKey(), scoreSnapshot.getValue(Integer.class));
                }
                callback.onScoresFetched(scores);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                callback.onError(error.getMessage());
            }
        });
    }

    public void fetchNumRounds(String lobbyPin, numRoundFetchCallback callback){

        DatabaseReference roundRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(lobbyPin).child("NumberOfRounds");

        roundRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String numRound = dataSnapshot.getValue(String.class);
                if (numRound !=null) {
                    callback.onNumRoundFetched(numRound);
                } else {
                    // Handle case where "rounds" node does not exist or is null
                    callback.onNumRoundFetched(numRound); // Assuming 0 as the default round number
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        });


    }


    @Override
    public void fetchPlayers(String pin, PlayerListUpdateCallback callback) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        lobbyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LobbyModel lobby = dataSnapshot.getValue(LobbyModel.class);
                if (lobby != null) {
                    List<String> playerNicknames = lobby.getPlayerNicknames();
                    callback.onPlayerListUpdated(playerNicknames);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        });
    }



    public void startGame(String pin) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        lobbyRef.child("gameStarted").setValue(true)
                .addOnSuccessListener(aVoid -> {
                    System.out.println("The game has started for lobby with pin: " + pin);
                })
                .addOnFailureListener(e -> {
                    System.err.println("Failed to start game for lobby: " + pin + ", error: " + e.getMessage());
                });
    }

    @Override
    public void isHost(String pin, String nickname, HostCheckCallback callback) {
        DatabaseReference lobbyRef = FirebaseDatabase.getInstance().getReference("lobbies").child(pin);

        lobbyRef.child("hostNickname").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String hostNickname = dataSnapshot.getValue(String.class);
                if (nickname.equals(hostNickname)) {
                    // If the nicknames match, the given nickname is the host
                    callback.onHostCheckResult(true);
                } else {
                    // The given nickname is not the host
                    callback.onHostCheckResult(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle possible errors
                System.err.println("Database error: " + databaseError.getMessage());
                callback.onError(databaseError.getMessage());
            }
        });
    }

    public void startRound(String pin) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        lobbyRef.child("roundStarted").setValue(true)
                .addOnSuccessListener(aVoid -> {
                    System.out.println("Round started for lobby with pin: " + pin);
                })
                .addOnFailureListener(e -> {
                    System.err.println("Failed to start round for lobby: " + pin + ", error: " + e.getMessage());
                });
    }

    @Override
    public void resetRoundFlag(String pin) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        lobbyRef.child("roundStarted").setValue(false)
                .addOnSuccessListener(aVoid -> {
                    System.out.println("Round flag reset for lobby with pin: " + pin);
                })
                .addOnFailureListener(e -> {
                    System.err.println("Failed to reset round flag for lobby: " + pin + ", error: " + e.getMessage());
                });
    }



    public void listenForGameStart(String pin, GameStartCallback callback) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);
        lobbyRef.child("gameStarted").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Boolean gameStarted = dataSnapshot.getValue(Boolean.class);
                if (gameStarted != null && gameStarted) {
                    callback.onGameStarted();
                    lobbyRef.child("gameStarted").removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle the error
            }
        });
    }

    @Override
    public long getSeed() {
        return 0;
        // Should return seed, will be done later
    }

    public void fetchWord(String pin, String category, WordFetchCallback callback) {
        DatabaseReference wordsRef = database.getReference("categories").child(category).child("words");
        wordsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> words = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    words.add(snapshot.getKey()); // Assuming the words are stored as keys
                }

                if (!words.isEmpty()) {
                    long seed = generateSeedFromPin(pin);
                    Collections.shuffle(words, new Random(seed));

                    String selectedWord = words.get(new Random().nextInt(words.size()));
                    callback.onWordFetched(selectedWord);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        });
    }


    public void checkIfAllPlayersAreFinished(String pin, AllPlayersFinishedCallback callback) {
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin).child("players");
        lobbyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean allPlayersFinished = true;
                for (DataSnapshot playerSnapshot : dataSnapshot.getChildren()) {
                    Boolean finished = playerSnapshot.child("finished").getValue(Boolean.class);
                    if (finished == null || !finished) {
                        allPlayersFinished = false;
                        break; // Exit if any player hasn't finished
                    }
                }
                callback.onResult(allPlayersFinished);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void updateFinishedFlag(String nickname, String pin) {
        // Reference to the specific lobby by its pin
        DatabaseReference lobbyRef = database.getReference("lobbies").child(pin);

        // Get the current state of the lobby to update the player's flag
        lobbyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LobbyModel lobby = dataSnapshot.getValue(LobbyModel.class);
                if (lobby != null) {
                    Map<String, PlayerModel> players = lobby.getPlayers();
                    PlayerModel player = players.get(nickname);
                    if (player != null) {
                        player.setFinished(true); // Set the player's flag to true
                        lobbyRef.setValue(lobby).addOnSuccessListener(aVoid -> {
                            // Successfully updated the player's flag
                            System.out.println("Player's finished flag updated successfully.");
                        }).addOnFailureListener(e -> {
                            // Handle failure to update the player's flag
                            System.err.println("Failed to update player's finished flag: " + e.getMessage());
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle database error
                System.err.println("Database error: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public void getPlayerAndScore(String pin, PlayerScoreCallback callback) {
        DatabaseReference playersRef = database.getReference("lobbies").child(pin).child("players");
        Map<String, Integer> playerScores = new HashMap<>(); // Initialize the map

        playersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot playerSnapshot : dataSnapshot.getChildren()) {
                    String playerName = playerSnapshot.getKey(); // Get player name
                    // Handle the case where score might not be an integer or could be null
                    Integer score = null;
                    try {
                        score = playerSnapshot.child("score").getValue(Integer.class); // Get player score
                    } catch (Exception e) {
                        System.out.println("Error parsing score for player " + playerName);
                    }
                    if (score != null) {
                        playerScores.put(playerName, score); // Add player score to the map
                    }
                }

                // Invoke the callback with the retrieved player scores
                callback.onPlayerScoresReceived(playerScores);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle database error
                System.out.println("Something is not okay");
            }
        });
    }

    public void updateRound(String pin, RoundUpdateCallback callback) {
        DatabaseReference roundsRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(pin).child("rounds");

        // Listen for the current round value once
        roundsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Integer currentRound = snapshot.getValue(Integer.class);
                if (currentRound == null) {
                    // If it's the first round, set to 1
                    roundsRef.setValue(1).addOnSuccessListener(aVoid -> {
                        System.out.println("Round initialized successfully to 1");
                        callback.onRoundUpdateSuccess();
                    }).addOnFailureListener(e -> {
                        System.err.println("Failed to initialize round: " + e.getMessage());
                        callback.onRoundUpdateFailure();
                    });
                } else {
                    // Increment the round and set the new value
                    int newRound = currentRound + 1;
                    roundsRef.setValue(newRound).addOnSuccessListener(aVoid -> {
                        System.out.println("Round updated successfully to " + newRound);
                        callback.onRoundUpdateSuccess();
                    }).addOnFailureListener(e -> {
                        System.err.println("Failed to update round: " + e.getMessage());
                        callback.onRoundUpdateFailure();
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.err.println("Failed to fetch current round: " + error.getMessage());
                callback.onRoundUpdateFailure();
            }
        });
    }


    @Override
    public void fetchRound(String pin, RoundCallback callback) {
        DatabaseReference roundRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(pin).child("rounds");

        roundRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Integer currentRound = dataSnapshot.getValue(Integer.class);
                if (currentRound != null) {
                    callback.onRoundFetched(currentRound);
                } else {
                    // Handle case where "rounds" node does not exist or is null
                    callback.onRoundFetched(0); // Assuming 0 as the default round number
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onError();
            }
        });
    }

    @Override
    public void resetFlags(String pin) {
        DatabaseReference playersRef = FirebaseDatabase.getInstance().getReference("lobbies")
                .child(pin).child("players");

        // Fetch all players under this pin
        playersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Iterate over all players
                for (DataSnapshot playerSnapshot : dataSnapshot.getChildren()) {
                    // Reset the "finished" flag for each player
                    playerSnapshot.getRef().child("finished").setValue(false)
                            .addOnSuccessListener(aVoid -> System.out.println("Reset finished flag for player " + playerSnapshot.getKey()))
                            .addOnFailureListener(e -> System.err.println("Failed to reset finished flag for player " + playerSnapshot.getKey() + ": " + e.getMessage()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.err.println("Failed to fetch players for resetting flags: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public void listenForRoundStarted(String pin, Runnable onRoundStarted) {
        DatabaseReference roundStartedRef = database.getReference("lobbies").child(pin).child("roundStarted");

        ValueEventListener roundStartedListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Boolean roundStarted = dataSnapshot.getValue(Boolean.class);
                if (Boolean.TRUE.equals(roundStarted)) {
                    // Round started, trigger the provided Runnable
                    onRoundStarted.run();

                    // Optionally reset the flag to false if it should only trigger once per round
                    roundStartedRef.setValue(false);

                    // Remove this listener to avoid it triggering again
                    roundStartedRef.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.err.println("Listening for round start cancelled: " + databaseError.getMessage());
            }
        };

        // Add the event listener to the roundStartedRef
        roundStartedRef.addValueEventListener(roundStartedListener);
    }



    private long generateSeedFromPin(String pin) {
        return Long.parseLong(pin);
    }



}
