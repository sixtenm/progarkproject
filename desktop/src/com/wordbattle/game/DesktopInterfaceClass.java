package com.wordbattle.game;


import com.wordbattle.game.model.Category;
import com.wordbattle.game.model.Word;
import com.wordbattle.game.network.FirebaseInterface;



public class DesktopInterfaceClass implements FirebaseInterface{

    @Override
    public void SomeFunction() {

    }

    @Override
    public void createNewLobby(String hostNickname, String pin, String numRounds) {

    }

    @Override
    public void joinLobby(String pin, String playerNickname, Runnable onSuccess, Runnable onFail) {

    }

    @Override
    public void fetchPlayers(String pin, PlayerListUpdateCallback callback) {

    }

    @Override
    public void startGame(String pin) {

    }

    @Override
    public void isHost(String pin, String nickname, HostCheckCallback callback) {

    }

    @Override
    public void startRound(String pin) {

    }

    @Override
    public void resetRoundFlag(String pin) {

    }

    @Override
    public void listenForGameStart(String pin, GameStartCallback callback) {

    }

    @Override
    public long getSeed() {
        return 0;
    }

    @Override
    public void fetchWord(String pin, String category, WordFetchCallback callback) {

    }

    @Override
    public void checkIfAllPlayersAreFinished(String pin, AllPlayersFinishedCallback callback) {

    }


    @Override
    public void updateFinishedFlag(String nickname, String pin) {

    }

    @Override
    public void getPlayerAndScore(String pin, PlayerScoreCallback callback) {

    }

    @Override
    public void updateRound(String pin, RoundUpdateCallback callback) {

    }

    @Override
    public void fetchRound(String pin, RoundCallback callback) {

    }

    @Override
    public void resetFlags(String pin) {

    }

    @Override
    public void listenForRoundStarted(String pin, Runnable onRoundStarted) {

    }

    @Override
    public void createNewCategory(Category category) {

    }

    @Override
    public void addWordToCategory(String categoryName, Word word) {

    }

    @Override
    public void addScoreToDatabase(String pin, String nickname) {

    }

    @Override
    public void updateScoreInDatabase(String pin, String nickname, int newScore, ScoreUpdateCallback callback) {

    }

    @Override
    public void fetchScores(String pin, ScoreFetchCallback callback) {

    }

    @Override
    public void fetchNumRounds(String lobbyPin, numRoundFetchCallback callback) {

    }

}
