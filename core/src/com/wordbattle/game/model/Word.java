package com.wordbattle.game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Word {

    private final String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }


    public List<String> getLetters(int amount) {
        List<String> letters = new ArrayList<>();

        // Add letters from the word
        for (char letter : word.toCharArray()) {
            letters.add(String.valueOf(letter));
        }

        // Generate random letters
        Random random = new Random();
        int maxLetters = 10;
        if (amount > maxLetters) {
            amount = maxLetters;
        }
        if (amount > 0) {
            for (int i = 0; i < amount; i++) {
                char randomLetter = (char) (random.nextInt(26) + 'a');
                letters.add(String.valueOf(randomLetter));
            }
        }

        Collections.shuffle(letters, random);

        for (String letter : letters) {
            System.out.print(letter + " ");
        }
        System.out.println();

        return letters;
    }

    public boolean checkWord(String guessedWord) {
        return guessedWord.equalsIgnoreCase(word);
    }

}


