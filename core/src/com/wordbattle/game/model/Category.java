package com.wordbattle.game.model;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String name;
    private List<Word> words;

    public Category(String name) {
        this.name = name;
        this.words = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Word> getWords() {
        return words;
    }

    public void addWord(Word word) {
        words.add(word);
    }
}
