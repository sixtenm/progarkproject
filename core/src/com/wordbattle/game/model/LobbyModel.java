// In the model package
package com.wordbattle.game.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LobbyModel {
    private String hostNickname;
    private String pin;
    private boolean gameStarted;
    private Map<String, PlayerModel> players; // Updated to use PlayerModel
    private List<String> playerNicknames; // Optional, depending on usage
    private Map<String, Integer> scores; // Optional, depending on usage
    private int rounds; // Add this line for the rounds counter

    public String getNumberOfRounds() {
        return NumberOfRounds;
    }

    public void setNumberOfRounds(String numberOfRounds) {
        NumberOfRounds = numberOfRounds;
    }

    private String NumberOfRounds;
    private boolean roundStarted;


    public LobbyModel() {
        // Initialize empty constructor to allow to create lobby from snapshot of DB
        playerNicknames = new ArrayList<>();
        scores = new HashMap<>();
        players = new HashMap<>();
        //numRounds="12";
        //numRounds = new String();
    }

    public LobbyModel(String hostNickname, String pin) {
        this.hostNickname = hostNickname;
        this.pin = pin;
        this.gameStarted = false;
        this.players = new HashMap<>();
        // Add the host player to the players map
        this.players.put(hostNickname, new PlayerModel(0, false)); // Initialize with default values
        this.rounds = 0; // Initialize the rounds counter
        //this.numRounds="13";
        this.NumberOfRounds="12";
        this.roundStarted = false;

    }

    public String getHostNickname() {
        return hostNickname;
    }

    public void setHostNickname(String hostNickname) {
        this.hostNickname = hostNickname;
    }

    public String getPin() {
        return pin;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }

    public Map<String, PlayerModel> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, PlayerModel> players) {
        this.players = players;
    }

    public List<String> getPlayerNicknames() {
        return new ArrayList<>(players.keySet()); // Convert player names to a list
    }

    public Map<String, Integer> getScores() {
        return scores;
    }

    public void setScores(Map<String, Integer> scores) {
        this.scores = scores;
    }

    public void addPlayer(String nickname, PlayerModel player) {
        players.put(nickname, player);
    }

    public void removePlayer(String nickname) {
        players.remove(nickname);
    }




    // Additional methods if needed

    @Override
    public String toString() {
        return "LobbyModel{" +
                "hostNickname='" + hostNickname + '\'' +
                ", pin='" + pin + '\'' +
                ", gameStarted=" + gameStarted +
                ", players=" + players +
                ", scores=" + scores +
                ", NumberOfRounds" + NumberOfRounds +
                '}';
    }
}
