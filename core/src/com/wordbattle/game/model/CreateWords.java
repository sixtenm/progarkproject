package com.wordbattle.game.model;
import com.badlogic.gdx.Gdx;
import com.wordbattle.game.model.Category;
import com.wordbattle.game.model.Word;
import com.wordbattle.game.network.FirebaseInterface;

import java.util.HashMap;
import java.util.Map;

public class CreateWords {
    Map<String, Category> categories;
    FirebaseInterface _FBIC;

    public CreateWords(FirebaseInterface _FBIC) {
        this.categories = new HashMap<>();
        this._FBIC = _FBIC;
    }

    public void updateDB() {
        try {
            String wrds = Gdx.files.internal("words.txt").readString();

            String[] lines = wrds.split("\n");

            for (String line : lines) {
                String[] parts = line.split(":");
                String categoryName = parts[0];
                String[] wordsArray = parts[1].split(",");

                System.out.println("Category: " + categoryName);
                Category category = new Category(categoryName);
                _FBIC.createNewCategory(category);
                for (String word : wordsArray) {
                    System.out.println("Word: " + word);
                    Word newWord = new Word(word);
                    _FBIC.addWordToCategory(category.getName(), newWord);
                }
            }

        }
     catch (Exception e) {
         e.printStackTrace();
        }
    }}
