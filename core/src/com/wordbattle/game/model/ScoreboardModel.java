package com.wordbattle.game.model;

import java.util.HashMap;
import java.util.Map;

public class ScoreboardModel {

    private Map<String, Integer> scores;

    public ScoreboardModel() {
        scores = new HashMap<>();
    }

    public void addPlayerToScoreboard(String nickname) {
        scores.put(nickname, 0); // Start with 0 score
    }

    // Call this when a player scores points
    public void updateScore(String nickname, int points) {
        if (scores.containsKey(nickname)) {
            scores.put(nickname, scores.get(nickname) + points);
        }
    }

    // Call this to get the score of a player
    public int getScore(String nickname) {
        return scores.getOrDefault(nickname, 0);
    }

    // Getter for the entire scores map
    public Map<String, Integer> getScores() {
        return scores;
    }


}


