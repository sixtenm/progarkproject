package com.wordbattle.game.model;


import com.badlogic.gdx.math.Rectangle;

import org.w3c.dom.css.Rect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class LetterPool {
    private Long seed;
    private String word;
    private ArrayList<Character> letters;
    private Random randomGenerator;
    private Rectangle rectangle;

    public LetterPool(String word, int extraLettersCount, String pin) {
        this.word = word.toUpperCase();
        letters = new ArrayList<Character>();
        seed = Long.parseLong(pin);
        this.randomGenerator = new Random(seed);



        // Add the word's letters to the pool
        for (char c : word.toCharArray()) {
            letters.add(c);
        }

        // Add extra random letters to the pool
        for (int i = 0; i < extraLettersCount; i++) {
            char randomChar = (char) ('A' + randomGenerator.nextInt(26));
            letters.add(Character.toUpperCase(randomChar));
        }

        // Shuffle the pool
        Collections.shuffle(letters);
    }

    public ArrayList<Character> getLetters() {
        return letters;
    }

}
