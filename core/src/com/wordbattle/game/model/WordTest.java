package com.wordbattle.game.model;

// Denne skal slettes

public class WordTest {

    public static void main(String[] args) {
        // Create an instance of Word
        Category category = new Category("Test");
        Word word = new Word("lemon");

        // Test the getWord method
        System.out.println("Word: " + word.getWord());

        // Test the getLetters method
        int numberOfRandomLetters = 3;
        System.out.println("Letters:");
        word.getLetters(numberOfRandomLetters);
        System.out.println(word.checkWord("lemon"));
    }
}