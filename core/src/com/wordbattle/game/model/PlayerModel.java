package com.wordbattle.game.model;

public class PlayerModel {
    private int score;
    private boolean finished;

    // Default constructor
    public PlayerModel() {
        // Default values for the fields
        this.score = 0;
        this.finished = false;
    }

    // Constructor with arguments
    public PlayerModel(int score, boolean finished) {
        this.score = score;
        this.finished = finished;
    }


    public int getScore() {
        return score;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}

