package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wordbattle.game.WordBattle;

public class MainMenuView {
    private Texture backgroundTexture;
    private Texture joinGameButton;
    private Texture newGameButton;
    private Texture howToPlayButton;
    private Rectangle joinGameButtonBounds;
    private Rectangle newGameButtonBounds;
    private Rectangle howToPlayButtonBounds;
    private Rectangle settingsBounds;
    private OrthographicCamera cam;


    public MainMenuView(OrthographicCamera cam) {
        this.cam = cam;
        this.backgroundTexture = new Texture("correctBg.png");
        this.joinGameButton = new Texture("join_game.png");
        this.newGameButton = new Texture("new_game.png");
        this.howToPlayButton = new Texture("how_to1.png");

        // Calculate bounds and assign them
        joinGameButtonBounds = new Rectangle(
                (WordBattle.WIDTH - joinGameButton.getWidth()) / 2,
                (WordBattle.HEIGHT / 2 - joinGameButton.getHeight() / 2 + 15) ,
                joinGameButton.getWidth(),
                joinGameButton.getHeight()
        );

        newGameButtonBounds = new Rectangle(
                (WordBattle.WIDTH - newGameButton.getWidth()) / 2,
                joinGameButtonBounds.y - newGameButton.getHeight() - 20, // Adjust y-position based on joinGameButton's position
                newGameButton.getWidth(),
                newGameButton.getHeight()
        );

        howToPlayButtonBounds = new Rectangle(
                (WordBattle.WIDTH - howToPlayButton.getWidth()) / 2,
                newGameButtonBounds.y - howToPlayButton.getHeight() - 10 , // Adjust y-position based on joinGameButton's position
                howToPlayButton.getWidth(),
                howToPlayButton.getHeight()
        );

        settingsBounds = new Rectangle(
                WordBattle.WIDTH - 100,
                WordBattle.HEIGHT - 100,
                100,
                100
        );
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();
        // Render background to fill the screen
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);
        // Render buttons using bounds for precise alignment
        spriteBatch.draw(joinGameButton, joinGameButtonBounds.x, joinGameButtonBounds.y);
        spriteBatch.draw(newGameButton, newGameButtonBounds.x, newGameButtonBounds.y);
        spriteBatch.draw(howToPlayButton, howToPlayButtonBounds.x, howToPlayButtonBounds.y);
        spriteBatch.end();
    }

    // Provide a way for the controller to access the bounds
    public Rectangle getJoinGameButtonBounds() {
        return joinGameButtonBounds;
    }

    public Rectangle getNewGameButtonBounds() {
        return newGameButtonBounds;
    }
    public Rectangle getHowToPlayButtonBounds() {
        return howToPlayButtonBounds;
    }
    public Rectangle getSettingsBounds() { return settingsBounds; }

    public void dispose() {
        backgroundTexture.dispose();
        joinGameButton.dispose();
        newGameButton.dispose();
        howToPlayButton.dispose();
    }
}

