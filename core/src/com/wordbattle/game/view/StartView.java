package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;

public class StartView {
    private OrthographicCamera cam;
    private Texture backgroundTexture;
    private Texture GuessTex;

    public StartView(OrthographicCamera cam) {
        this.cam = cam;
        backgroundTexture=new Texture("bg2.png");
        GuessTex = new Texture("Guess!.png");
    }

    public void render(SpriteBatch spriteBatch){
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();
        // Render background to fill the screen
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);
        spriteBatch.draw(GuessTex,10,320);

        spriteBatch.end();
    }

    public void dispose(){

    }
}
