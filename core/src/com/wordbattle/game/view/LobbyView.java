package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.wordbattle.game.WordBattle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.List;


public class LobbyView {
    private Texture backgroundTexture;
    private BitmapFont font;
    private OrthographicCamera cam;
    private String pin;
    private ImageButton startGameButton;
    private Stage stage;
    private Texture startGameTexture; // Texture for the "START GAME" button background

    private Rectangle startTheGameBounds;
    private List<String> playerNames = new ArrayList<>();

    public LobbyView(OrthographicCamera cam, String pin) {
        this.cam = cam;
        this.pin = pin;

        stage = new Stage(new ScreenViewport(cam));
        Gdx.input.setInputProcessor(stage);

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        font = generator.generateFont(parameter);
        generator.dispose();

        // Create start game button with background texture
        startGameTexture = new Texture("Golden_long-01.png");
        TextureRegionDrawable buttonBackground = new TextureRegionDrawable(new TextureRegion(startGameTexture));
        ImageButton.ImageButtonStyle buttonStyle = new ImageButton.ImageButtonStyle();
        buttonStyle.up = buttonBackground;
        startGameButton = new ImageButton(buttonStyle);
        startGameButton.setPosition(WordBattle.WIDTH / 2 - startGameButton.getWidth() / 2, 100);
        stage.addActor(startGameButton);

        startTheGameBounds = new Rectangle((WordBattle.WIDTH / 2) - (startGameTexture.getWidth() / 2), 100, startGameTexture.getWidth(), startGameTexture.getHeight());
        // Load background texture
        backgroundTexture = new Texture("bg2.png");
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        font.setColor(Color.valueOf("E456CE")); // Set text color to E456CE

        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);

        // Calculate the width of the text
        float waitingForWidth = "WAITING FOR".length() * 14; // Approximate width per character is 14
        float playersToJoinWidth = "PLAYERS TO JOIN...".length() * 14;

        if (playerNames.size() < 2 ) {
            // Draw "WAITING FOR" in the middle of the screen
            font.draw(spriteBatch, "WAITING FOR", (WordBattle.WIDTH - waitingForWidth) / 2, 500);
            // Draw "PLAYERS TO JOIN..." in the middle of the screen
            font.draw(spriteBatch, "PLAYERS TO JOIN...", (WordBattle.WIDTH - playersToJoinWidth) / 2, 450);
        }
        else {
            // Draw the list of players
            float startY = WordBattle.HEIGHT / 2f; // Adjust Y as necessary
            for (String playerName : playerNames) {
                font.draw(spriteBatch, playerName, WordBattle.WIDTH / 2f, startY);
                startY -= font.getLineHeight(); // Space the player names out
            }
        }

        // Draw PIN
        font.draw(spriteBatch, "PIN: " + pin, 160, 750);;

        spriteBatch.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        // Begin another batch to draw the text
        spriteBatch.begin();
        font.setColor(Color.valueOf("000000")); // Set text color to E456CE
        GlyphLayout layout = new GlyphLayout(font, "START GAME");
        float textWidth = layout.width; // Get the width of the text
        float textHeight = layout.height; // Get the height of the text
        float buttonWidth = startGameButton.getWidth(); // Get the width of the button
        float buttonHeight = startGameButton.getHeight(); // Get the height of the button
        float textX = startGameButton.getX() + (buttonWidth - textWidth) / 2; // Calculate the X position
        float textY = startGameButton.getY() + (buttonHeight + textHeight) / 2; // Calculate the Y position
        font.draw(spriteBatch, "START GAME", textX, textY);
        spriteBatch.end();
    }


    public void dispose() {
        stage.dispose();
        font.dispose();
        backgroundTexture.dispose();
        startGameTexture.dispose(); // Dispose the start game button texture
    }

    public ImageButton getStartGameButton() {
        return startGameButton;
    }

    public Rectangle getStartGameButtonBounds() {
        return startTheGameBounds;
    }


    public void setPlayerNames(List<String> playerNames) {
        this.playerNames = playerNames;
    }
}
