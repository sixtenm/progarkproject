package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.wordbattle.game.WordBattle;

import java.util.Random;

public class CreateGameView {
    private Texture easyPinkTexture;
    private Texture mediumPinkTexture;
    private Texture hardPinkTexture;
    private Texture easyGoldenTexture;

    private Texture mediumGoldenTexture;

    private Texture hardGoldenTexture;

    private Texture backgroundTexture;
    private Texture nicknameTexture; // If you have a texture for the nickname field
    private Texture createGameTexture;

    private Texture purpleCircleTex;

    private Texture goldenCircleTex;
    private Rectangle nicknameBounds; // Bounds for the nickname field for input detection
    private Rectangle createGameBounds;
    private Rectangle easyButtonBounds;
    private Rectangle mediumButtonBounds;
    private Rectangle hardButtonBounds;
    private Stage stage;
    private TextField nicknameField;
    private BitmapFont font;
    private OrthographicCamera cam;
    private String pin;
    private String selectedLevel = "easy"; // default to easy
    private String numRounds="3";
    private String nickNameMessage;
    private Rectangle settingsBounds;
    private Rectangle goBackButtonBounds;
    private Rectangle round3ButtonBounds;
    private Rectangle round5ButtonBounds;
    private Rectangle round10ButtonBounds;


    public CreateGameView(OrthographicCamera cam) {
        this.cam = cam;
        stage = new Stage(new ScreenViewport(cam));
        Gdx.input.setInputProcessor(stage);

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        font = generator.generateFont(parameter);
        generator.dispose();

        // Initialize TextFieldStyle with the font
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.BLACK;

        goBackButtonBounds = new Rectangle(
                0,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        settingsBounds = new Rectangle(
                WordBattle.WIDTH - 100,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        // Create the TextField for nickname input
        nicknameField = new TextField("", textFieldStyle);
        nicknameField.setMessageText("Enter Nickname");
        nicknameField.setAlignment(1); // 1 = Center
        nicknameField.setMaxLength(15); // Max chars set to 15

        settingsBounds = new Rectangle(
                WordBattle.WIDTH - 100,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        goBackButtonBounds = new Rectangle(
                0,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        // Generate a random PIN
        pin = generateRandomPin();

        // Load textures
        backgroundTexture = new Texture("bg_btn_settings.png");
        nicknameTexture = new Texture("pink_long-01.png");
        createGameTexture = new Texture("Golden_long-01.png");
        // Levels, non-selected
        easyPinkTexture = new Texture("pink_short-01.png");
        mediumPinkTexture = new Texture("pink_short-01.png");
        hardPinkTexture = new Texture("pink_short-01.png");
        // Levels, selected
        easyGoldenTexture = new Texture("golden_short-01.png");
        mediumGoldenTexture = new Texture("golden_short-01.png");
        hardGoldenTexture = new Texture("golden_short-01.png");

        goldenCircleTex = new Texture("small_yellow_circle.png");
        purpleCircleTex = new Texture("small_pink_circle.png");


        // Define bounds for interactive buttons
        nicknameBounds = new Rectangle((WordBattle.WIDTH / 2) - (nicknameTexture.getWidth() / 2), 550, nicknameTexture.getWidth(), nicknameTexture.getHeight());
        createGameBounds = new Rectangle((WordBattle.WIDTH / 2) - (createGameTexture.getWidth() / 2), 100, createGameTexture.getWidth(), createGameTexture.getHeight());

        easyButtonBounds = new Rectangle(((WordBattle.WIDTH / 3)*1) - (easyPinkTexture.getWidth() / 2) - 80, 400, easyPinkTexture.getWidth(), easyPinkTexture.getHeight());
        mediumButtonBounds = new Rectangle(((WordBattle.WIDTH / 3)*2) - (easyPinkTexture.getWidth() / 2) - 80, 400, easyPinkTexture.getWidth(), easyPinkTexture.getHeight());
        hardButtonBounds = new Rectangle(((WordBattle.WIDTH / 3)*3) - (easyPinkTexture.getWidth() / 2) - 80 , 400, easyPinkTexture.getWidth(), easyPinkTexture.getHeight());

        round3ButtonBounds=new Rectangle(100, 250, purpleCircleTex.getWidth(),purpleCircleTex.getHeight());
        round5ButtonBounds=new Rectangle(200, 250, purpleCircleTex.getWidth(),purpleCircleTex.getHeight());
        round10ButtonBounds=new Rectangle(300, 250,purpleCircleTex.getWidth(),purpleCircleTex.getHeight());


        // Set the position and size of the nicknameField
        nicknameField.setPosition(nicknameBounds.x, nicknameBounds.y + 5);
        nicknameField.setSize(nicknameBounds.width, nicknameBounds.height);
        stage.addActor(nicknameField);

        nickNameMessage="";

    }
    public Rectangle getSettingsBounds() { return settingsBounds; }

    private String generateRandomPin() {
        return String.format("%04d", new Random().nextInt(10000));
    }

    public Rectangle getGoBackButtonBounds() {
        return goBackButtonBounds;
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);

        // Draw the PIN and nickname textures at their designated positions
        // spriteBatch.draw(pinTexture, pinBounds.x, pinBounds.y);
        spriteBatch.draw(nicknameTexture, nicknameBounds.x, nicknameBounds.y);
        spriteBatch.draw(createGameTexture, createGameBounds.x, createGameBounds.y);

        spriteBatch.draw(selectedLevel.equals("easy") ? easyGoldenTexture : easyPinkTexture, easyButtonBounds.x, easyButtonBounds.y);
        spriteBatch.draw(selectedLevel.equals("medium") ? mediumGoldenTexture : mediumPinkTexture, mediumButtonBounds.x, mediumButtonBounds.y);
        spriteBatch.draw(selectedLevel.equals("hard") ? hardGoldenTexture : hardPinkTexture, hardButtonBounds.x, mediumButtonBounds.y );

        spriteBatch.draw(numRounds.equals("3") ? goldenCircleTex:purpleCircleTex,round3ButtonBounds.x,round3ButtonBounds.y);
        spriteBatch.draw(numRounds.equals("5") ? goldenCircleTex:purpleCircleTex,round5ButtonBounds.x,round5ButtonBounds.y);
        spriteBatch.draw(numRounds.equals("10") ? goldenCircleTex:purpleCircleTex,round10ButtonBounds.x,round10ButtonBounds.y);

        // Render the PIN and nickname text
        font.setColor(Color.WHITE);
        font.draw(spriteBatch,"Pin: " + pin, 160, 750);
        font.setColor(Color.BLACK);
        font.draw(spriteBatch, "Create Game", createGameBounds.x + (createGameBounds.width / 2) - 100, createGameBounds.y + (createGameBounds.height / 2) + 20);
        font.draw(spriteBatch, "Easy", easyButtonBounds.x + (easyButtonBounds.width / 2) - 35, easyButtonBounds.y + (easyButtonBounds.height / 2) + 20);
        font.draw(spriteBatch, "Medium", mediumButtonBounds.x + (mediumButtonBounds.width / 2) - 60, mediumButtonBounds.y + (mediumButtonBounds.height / 2) + 20);
        font.draw(spriteBatch, "Hard", hardButtonBounds.x + (hardButtonBounds.width / 2) - 35, hardButtonBounds.y + (hardButtonBounds.height / 2) + 20);
        font.setColor(Color.PINK);
        font.draw(spriteBatch,"Number of rounds", 90,hardButtonBounds.y + (hardButtonBounds.height / 2) -60);
        if (!nickNameMessage.isEmpty()){
            font.draw(spriteBatch,nickNameMessage,nicknameBounds.x+120,nicknameBounds.y);
        }
        font.setColor(Color.BLACK);
        font.draw(spriteBatch,"3",round3ButtonBounds.x+round3ButtonBounds.width/2 -14,round3ButtonBounds.y + round3ButtonBounds.height/2 +20 );
        font.draw(spriteBatch,"5",round5ButtonBounds.x+round5ButtonBounds.width/2 - 14,round5ButtonBounds.y + round5ButtonBounds.height/2 +20 );
        font.draw(spriteBatch,"10",round10ButtonBounds.x+round10ButtonBounds.width/2 -14,round10ButtonBounds.y + round10ButtonBounds.height/2 +20 );


        spriteBatch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    public void dispose() {
        stage.dispose();
        font.dispose();
    }

    // Methods to access the PIN and nickname values
    public String getPin() {
        return pin;
    }

    public String getNickname() {
        return nicknameField.getText();
    }

    public Rectangle getEasyButtonBounds() {
        return easyButtonBounds;
    }

    public void setSelectedLevel(String level) {
        this.selectedLevel = level;
    }

    public void setNumRounds(String numRounds) {
        this.numRounds = numRounds;
    }

    public Rectangle getMediumButtonBounds() {
        return mediumButtonBounds;
    }


    public Rectangle getHardButtonBounds() {
        return hardButtonBounds;
    }


    public Rectangle getRound3ButtonBounds() {
        return round3ButtonBounds;
    }

    public Rectangle getRound5ButtonBounds() {
        return round5ButtonBounds;
    }

    public Rectangle getRound10ButtonBounds() {
        return round10ButtonBounds;
    }

    public String getNumRounds(){
        return numRounds;
    }

    public String returnBound () {
        return easyPinkTexture.getWidth() + ", and " + easyPinkTexture.getHeight();
    }
    public Rectangle getCreateGameBounds() {
        return createGameBounds;
    }

    public void setNickNameMessage(String nickNameMessage) {
        this.nickNameMessage = nickNameMessage;
    }
}




