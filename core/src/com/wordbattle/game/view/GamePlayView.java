package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;
import com.sun.tools.sjavac.Log;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.model.LetterPool;
import com.wordbattle.game.network.FirebaseInterface;

import org.w3c.dom.ranges.Range;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

import jdk.internal.org.jline.terminal.Size;

public class GamePlayView {

    private FirebaseInterface _FBIC;
    private OrthographicCamera cam;
    private Stage stage;
    private String currentWord;
    private ShapeRenderer shapeRenderer;
    private Texture backgroundTexture;
    private LetterPool letterPool;
    private BitmapFont font;
    private String pin;
    private Stack<Character> selectedLetters = new Stack<>();
    private ArrayList<Character> letters;
    private Rectangle[] letterBoxBounds;
    private Rectangle[] letterBounds;
    private Character[] filledBoxes;
    private float timeLeft;
    private float maxTime;
    private boolean isTimerRunning;
    private Rectangle settingsBounds;

    private ArrayList<Integer> YOffsets;//used to make letters look scattered

    public GamePlayView(OrthographicCamera cam, FirebaseInterface _FBIC, String pin) {
        this._FBIC = _FBIC;
        this.pin = pin;
        this.cam = cam;
        stage = new Stage(new ScreenViewport(cam));
        Gdx.input.setInputProcessor(stage);

        System.out.println("message from constructor");

        settingsBounds = new Rectangle(
                WordBattle.WIDTH + 400,
                WordBattle.HEIGHT + 1400,
                150,
                150
        );

        // Load BG
        backgroundTexture = new Texture("bgWithSettings.png");

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 120;
        font = generator.generateFont(parameter);
        generator.dispose();

        // Initialize TextFieldStyle with the font
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.WHITE;

        // Set timer
        maxTime = 30.0f;
        timeLeft = 30.0f; // Starting with 30 seconds for example
        isTimerRunning = true; // Start with the timer running
    }

    public Rectangle getSettingsBounds() { return settingsBounds; }

    // Update timer
    public void update(float deltaTime) {
        if (isTimerRunning) {
            timeLeft -= deltaTime;
            if (timeLeft <= 0) {
                timeLeft = 0;
                isTimerRunning = false; // Stop the timer when it reaches 0
                // Handle time out logic
            }
        }
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
        letterPool = new LetterPool(currentWord, 5, pin); // Or however you decide the number of extra letters
        letterBoxBounds = new Rectangle[currentWord.length()];
        filledBoxes = new Character[currentWord.length()];
        Random rand = new Random();
        YOffsets= new ArrayList<>(letterPool.getLetters().size());
        for (int i=0; i<letterPool.getLetters().size();i++){
            YOffsets.add(rand.nextInt(200)-100);
        }
    }

    public void render(SpriteBatch sb) {
        if (shapeRenderer == null) {
            shapeRenderer = new ShapeRenderer();
        }
        if (letterPool != null) {
            letters = letterPool.getLetters();
            letterBounds = new Rectangle[letters.size()];

            float letterSpacing = 100;
            float totalWidthLetter = ((((float) letters.size() * letterSpacing)) / 2);
            //float letterStartX = ((cam.viewportWidth - totalWidthLetter) - 2*letterSpacing) / 2;
            float letterStartX=100;
            float letterStartY = cam.viewportHeight / 2;
            sb.setProjectionMatrix(cam.combined);
            sb.begin(); // Begin the SpriteBatch for other rendering
            sb.draw(backgroundTexture, 0, 0, cam.viewportWidth, cam.viewportHeight);

            float letterStartYBaseEven = 700; // Midpoint for range 500 to 900
            float letterStartYBaseOdd = -550; // Midpoint for range 300 to 800
            int num=0;
            float yPos=0;

            for (int i = 0; i < letters.size(); i++) {

                // Calculate the position for each letter
                float xPos = (float) (letterStartX + i *letterSpacing/2);

                if (num == 0) {
                    // For even indices, use the range 500 to 900
                    yPos = letterStartY + 750 + YOffsets.get(i); // Alternate within the range
                }
                if(num==1) {
                    // For odd indices, use the range 300 to 800
                    yPos = letterStartY +400 + YOffsets.get(i);// Alternate within the range
                }
                if(num==2){
                    yPos = letterStartY -350 + YOffsets.get(i);// Alternate within the range
                }
                if(num==3){
                    yPos = letterStartY -750 + YOffsets.get(i);// Alternate within the range
                    num=-1;
                }

                // Draw each letter, here you might want to draw a background texture for the letter
                font.setColor(Color.GREEN);
                font.draw(sb, letters.get(i).toString(), xPos, yPos);
                letterBounds[i] = new Rectangle(xPos, yPos - 120, 120, 120);
                num++;
            }

            // Draw time
            font.draw(sb, "Time: " + String.format("%o", Math.round(timeLeft)), cam.viewportWidth / 2 - 200, cam.viewportHeight - 200);

            sb.end(); // End SpriteBatch

            // This is the rects around letters
            shapeRenderer.setProjectionMatrix(cam.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line); // Begin ShapeRenderer
            for (int i = 0; i < getLetterBounds().length; i++) {
                Rectangle rect = getLetterBounds()[i];
                shapeRenderer.rect(rect.x, rect.y, rect.width, rect.height);
            }
            shapeRenderer.end(); // End ShapeRenderer
            shapeRenderer.setProjectionMatrix(cam.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(Color.WHITE); // Set the color for the boxes

            float boxWidth = 120; // Set the width for the boxes
            float boxHeight = 120; // Set the height for the boxes
            float spacing = 25; // Space between boxes
            float totalWidth = currentWord.length() * (boxWidth + spacing) - spacing;
            float startX = (cam.viewportWidth - totalWidth) / 2; // Center the boxes
            float yPosition = cam.viewportHeight / 2; // Y position for the boxes

            for (int i = 0; i < currentWord.length(); i++) {
                float boxX = startX + i * (boxWidth + spacing);
                shapeRenderer.rect(boxX, yPosition, boxWidth, boxHeight);
                // Add a new box to arr
                letterBoxBounds[i] = new Rectangle(boxX, yPosition, boxWidth, boxHeight);
            }

            shapeRenderer.end();
            sb.begin();

            // This is letters being drawn in the boxes
            for (int i = 0; i < filledBoxes.length; i++) {
                if (filledBoxes[i] != null) {
                    // Draw the letter in the box
                    float y = getLetterBoxBounds()[i].y + 120;
                    float x = getLetterBoxBounds()[i].x + 10;
                    if (isWordComplete() && !isWordCorrect()) {
                        font.setColor(Color.RED);
                    }
                    font.draw(sb, filledBoxes[i].toString(), x, y);
                }
            }
            if (isWordComplete() && !isWordCorrect()) {
                float textX = (float) (WordBattle.WIDTH) / 2;
                float textY = (float) WordBattle.HEIGHT + 350;
                font.draw(sb, "Wrong word", textX, textY);
            } else {
                font.setColor(Color.GREEN);
            }
            sb.end();
        }
    }

    public Stack<Character> getSelectedLetters() {
        return selectedLetters;
    }

    public ArrayList<Character> getLetters() {
        return letters;
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public Rectangle[] getLetterBounds() {
        return letterBounds;
    }

    public Rectangle[] getLetterBoxBounds() {
        return letterBoxBounds;
    }

    public Character[] getFilledBoxes() {
        return filledBoxes;
    }

    public void setFilledBoxes(Character[] filledBoxes) {
        this.filledBoxes = filledBoxes;
    }

    public void dispose() {
        shapeRenderer.dispose();
    }

    // Timer control methods
    public void startTimer() {
        isTimerRunning = true;
    }

    public void stopTimer() {
        isTimerRunning = false;
    }

    public void resetTimer(float newTime) {
        timeLeft = newTime;
    }

    public float getTimeLeft() {
        return timeLeft;
    }

    public float getMaxTime() {
        return maxTime;
    }

    private boolean isWordComplete() {
        // Check if all boxes are filled
        for (Character letter : filledBoxes) {
            if (letter == null) {
                return false; // Found an empty box, so the word isn't complete
            }
        }
        return true; // All boxes are filled
    }

    private boolean isWordCorrect() {
        if (isWordComplete()) {
            // Construct the word from filled boxes
            StringBuilder userWord = new StringBuilder();
            for (Character letter : filledBoxes) {
                userWord.append(letter);
            }
            // Check if the constructed word matches the current word
            return userWord.toString().equals(currentWord);
        }
        return false; // Word is not complete, so it cannot be correct
    }

}