package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.wordbattle.game.WordBattle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class JoinGameView {

    private Texture backgroundTexture;
    private Texture joinGameButton;

    private Texture nicknameTexture;

    private Texture pinTexture;
    private Rectangle joinGameButtonBounds;

    private Rectangle pinButtonBounds;

    private Rectangle nicknameBounds;
    private OrthographicCamera cam;
    private Stage stage;

    private BitmapFont font;

    private TextField nicknameField;

    private TextField pinField;

    private String pinMessage;

    private String nicknameMessage;
    private String errorMsg;
    private String errorMessage = "";
    private Rectangle settingsBounds;
    private Rectangle goBackButtonBounds;

    private String lobbyIsFullMessage;


    public JoinGameView(OrthographicCamera cam){
        this.cam = cam;
        stage = new Stage(new ScreenViewport(cam));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture("bg_btn_settings.png");
        nicknameTexture = new Texture("pink_long-01.png");
        pinTexture = new Texture("pink_long-01.png");
        joinGameButton = new Texture("join_game.png");

        goBackButtonBounds = new Rectangle(
                0,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        joinGameButtonBounds = new Rectangle(
                (WordBattle.WIDTH - joinGameButton.getWidth()) / 2,
                (WordBattle.HEIGHT / 2 - joinGameButton.getHeight() / 2) -160,
                joinGameButton.getWidth(),
                joinGameButton.getHeight()-40 //rectangle is larger than button for some reason
        );

        pinButtonBounds= new Rectangle(
                (WordBattle.WIDTH - pinTexture.getWidth()) / 2,
                (WordBattle.HEIGHT / 2 - pinTexture.getHeight() / 2) +0,
                pinTexture.getWidth(),
                pinTexture.getHeight()-40
        );

        nicknameBounds = new Rectangle(
                (WordBattle.WIDTH - nicknameTexture.getWidth()) / 2,
                (WordBattle.HEIGHT / 2 - nicknameTexture.getHeight() / 2)+160,
                nicknameTexture.getWidth(),
                nicknameTexture.getHeight()-40
        );

        settingsBounds = new Rectangle(
                WordBattle.WIDTH - 100,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        lobbyIsFullMessage="";

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        font = generator.generateFont(parameter);
        generator.dispose();

        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.BLACK;

        nicknameField = new TextField("", textFieldStyle);
        nicknameField.setMessageText("Enter Nickname");
        nicknameField.setAlignment(1); // 1 = Center
        nicknameField.setMaxLength(15); // Max chars set to 15

        pinField = new TextField("", textFieldStyle);
        pinField.setMessageText("Enter Pin");
        pinField.setAlignment(1); // 1 = Center
        pinField.setMaxLength(4); // Max chars set to 4

        addTextFieldListeners();

        // Set the position and size of the nicknameField
        nicknameField.setPosition(nicknameBounds.x, nicknameBounds.y + 5);
        nicknameField.setSize(nicknameBounds.width, nicknameBounds.height);
        stage.addActor(nicknameField);

        // Set the position and size of the nicknameField
        pinField.setPosition(pinButtonBounds.x, pinButtonBounds.y + 5);
        pinField.setSize(pinButtonBounds.width, pinButtonBounds.height);
        stage.addActor(pinField);

        nicknameMessage="";
        pinMessage="";
    }

    //help for android simulator on pc to get keyboard down
    private void addTextFieldListeners() {
        nicknameField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (c == '\n') {
                    Gdx.input.setOnscreenKeyboardVisible(false);
                }
            }
        });

        pinField.setTextFieldListener(new TextField.TextFieldListener() {
            @Override
            public void keyTyped(TextField textField, char c) {
                if (c == '\n') {
                    Gdx.input.setOnscreenKeyboardVisible(false);
                }
            }
        });
    }

    public Rectangle getGoBackButtonBounds() {
        return goBackButtonBounds;
    }

    public Rectangle getSettingsBounds() { return settingsBounds; }

    public void render(SpriteBatch spriteBatch){
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.begin();

        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);

        spriteBatch.draw(joinGameButton, joinGameButtonBounds.x, joinGameButtonBounds.y);
        spriteBatch.draw(pinTexture,pinButtonBounds.x, pinButtonBounds.y);
        spriteBatch.draw(nicknameTexture, nicknameBounds.x, nicknameBounds.y);

        if (!errorMessage.isEmpty()) {
            font.draw(spriteBatch, errorMessage, pinButtonBounds.x + pinButtonBounds.width / 4, pinButtonBounds.y + 32);
        }
        if (!nicknameMessage.isEmpty()) {
            font.draw(spriteBatch, nicknameMessage, nicknameBounds.x - 12, nicknameBounds.y + 32);
        }
        if(!lobbyIsFullMessage.isEmpty()){
            font.draw(spriteBatch,lobbyIsFullMessage, joinGameButtonBounds.x+50,joinGameButtonBounds.y-50 );
        }

        spriteBatch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    public Rectangle getJoinGameButtonBounds() {
        return joinGameButtonBounds;
    }

    public String getPin(){
        return pinField.getText();
    }

    public String getNickname(){
        return nicknameField.getText();
    }

    public void setPinMessage(String pinMessage) {
        this.pinMessage = pinMessage;
    }

    public void setNicknameMessage(String nicknameMessage) {
        this.nicknameMessage = nicknameMessage;
    }

    public void dispose() {
        backgroundTexture.dispose();
        joinGameButton.dispose();
        pinTexture.dispose();
        nicknameTexture.dispose();
    }

    public void setErrorMessage(String s) {
        errorMessage = s;
    }

    public void setLobbyIsFullMessage(String lobbyIsFullMessage) {
        this.lobbyIsFullMessage = lobbyIsFullMessage;
    }
}
