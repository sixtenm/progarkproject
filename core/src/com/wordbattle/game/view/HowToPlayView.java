package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.network.FirebaseInterface;

public class HowToPlayView {
    private Texture backgroundTexture;
    private Texture showHintTexture;
    private Rectangle invisibleButtonHintBounds;
    private Rectangle invisibleButtonGoBackBounds;
    private OrthographicCamera cam;
    private boolean showHint;

    public HowToPlayView(OrthographicCamera cam, FirebaseInterface _FBIC) {
        this.cam = cam;
        this.backgroundTexture = new Texture("howToPlayBg.png");
        this.showHintTexture = new Texture("showHint.png");
        this.showHint = false;

        invisibleButtonHintBounds = new Rectangle(
                WordBattle.WIDTH - 100,
                0,
                100,
                100
        );

        invisibleButtonGoBackBounds = new Rectangle(
                0,
                WordBattle.HEIGHT - 100,
                100,
                100
        );
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();

        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);

        if (showHint) {
            spriteBatch.draw(showHintTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);
        }

        spriteBatch.end();
    }

    public void toggleShowHint() {
        showHint = !showHint;
    }

    public Rectangle getInvisibleButtonHintBounds() {
        return invisibleButtonHintBounds;
    }

    public Rectangle getInvisibleButtonGoBackBounds() {
        return invisibleButtonGoBackBounds;
    }

    public void dispose() {
        backgroundTexture.dispose();
        showHintTexture.dispose();
    }
}
