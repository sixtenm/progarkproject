package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.network.FirebaseInterface;

import java.util.ArrayList;
import java.util.List;

public class WaitForHostView {
    private Texture backgroundTexture;

    private OrthographicCamera cam;
    private FirebaseInterface _FBIC;
    private String nickname;
    private String pin;
    private List<String> playerNames = new ArrayList<>();
    private Stage stage;
    private BitmapFont font;

    public WaitForHostView(OrthographicCamera cam, FirebaseInterface _FBIC, String nickname, String pin){
        this._FBIC = _FBIC;
        this.nickname = nickname;
        this.pin = pin;
        this.cam = cam;

        stage = new Stage(new ScreenViewport(cam));
        Gdx.input.setInputProcessor(stage);

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        font = generator.generateFont(parameter);
        generator.dispose();

        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = font;
        textFieldStyle.fontColor = Color.WHITE;

        backgroundTexture = new Texture("bg2.png");
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);

        // Text on screen
        font.draw(spriteBatch, "Pin: " + pin, WordBattle.WIDTH / 2f, WordBattle.HEIGHT - 50);

        font.draw(spriteBatch, "Waiting for host to start the game...", WordBattle.WIDTH / 2f, 50);

        // Draw the list of players
        float startY = WordBattle.HEIGHT / 2f; // Adjust Y as necessary
        for (String playerName : playerNames) {
            font.draw(spriteBatch, playerName, WordBattle.WIDTH / 2f, startY);
            startY -= font.getLineHeight(); // Space the player names out
        }

        spriteBatch.end();
    }

    public void setPlayerNames(List<String> playerNames) {
        this.playerNames = playerNames;
    }


    public OrthographicCamera getCam() {
        return cam;
    }

    public void dispose(){


    }
}
