package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.manager.MusicManager;
import com.wordbattle.game.manager.SoundManager;

public class SettingsView {

    private Texture backgroundTexture;
    private Texture quitGameTexture;
    private Texture switchOnMusicTexture;
    private Texture switchOffMusicTexture;
    private Texture switchOnSoundTexture;
    private Texture switchOffSoundTexture;
    private Rectangle switchMusicBounds;
    private Rectangle switchSoundBounds;

    private OrthographicCamera cam;
    private Rectangle goBackButtonBounds;
    private Rectangle quitGameButtonBounds;

    public SettingsView(OrthographicCamera cam) {
        this.cam = cam;
        this.backgroundTexture = new Texture("settingsBg1.png");
        this.quitGameTexture = new Texture("quitGameBtn.png");
        this.switchOnMusicTexture = new Texture("onButton.png");
        this.switchOffMusicTexture = new Texture("offButton.png");
        this.switchOnSoundTexture = new Texture("onButton.png");
        this.switchOffSoundTexture = new Texture("offButton.png");

        goBackButtonBounds = new Rectangle(
                0,
                WordBattle.HEIGHT - 100,
                100,
                100
        );

        float buttonWidth = 300;
        float buttonHeight = 100;
        quitGameButtonBounds = new Rectangle(
                (WordBattle.WIDTH - buttonWidth) / 2,
                WordBattle.HEIGHT / 3 - buttonHeight / 2,
                buttonWidth,
                buttonHeight
        );

        //Music button
        float switchWidth = 100;
        float switchHeight = 45;
        float switchX = WordBattle.WIDTH - switchWidth - 85;
        float switchY = WordBattle.HEIGHT - switchHeight - 236;
        switchMusicBounds = new Rectangle(
                switchX,
                switchY,
                switchWidth,
                switchHeight
        );
        //Sound button
        float switchSoundX = switchX;
        float switchSoundY = switchY - 122;
        switchSoundBounds = new Rectangle(
                switchSoundX,
                switchSoundY,
                switchWidth,
                switchHeight
        );
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();


        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, WordBattle.WIDTH, WordBattle.HEIGHT);
        spriteBatch.draw(quitGameTexture, quitGameButtonBounds.x, quitGameButtonBounds.y,
                quitGameButtonBounds.width, quitGameButtonBounds.height);
        //test with manager
        Texture switchMusicTexture = MusicManager.isMusicOn() ? switchOnMusicTexture : switchOffMusicTexture;
        spriteBatch.draw(switchMusicTexture, switchMusicBounds.x, switchMusicBounds.y,
                switchMusicBounds.width, switchMusicBounds.height);

        Texture switchSoundTexture = SoundManager.isSoundOn() ? switchOnSoundTexture : switchOffSoundTexture;
        spriteBatch.draw(switchSoundTexture, switchSoundBounds.x, switchSoundBounds.y,
                switchSoundBounds.width, switchSoundBounds.height);
        spriteBatch.end();
    }

    public Rectangle getGoBackButtonBounds() {
        return goBackButtonBounds;
    }
    public Rectangle getQuitGameButtonBounds() {
        return quitGameButtonBounds;
    }
    public Rectangle getSwitchMusicBounds() {
        return switchMusicBounds;
    }

    public Rectangle getSwitchSoundBounds() {
        return switchSoundBounds;
    }

    public void dispose() {
        backgroundTexture.dispose();
        quitGameTexture.dispose();
        switchOnMusicTexture.dispose();
        switchOffMusicTexture.dispose();
        switchOnSoundTexture.dispose();
        switchOffSoundTexture.dispose();
    }
}