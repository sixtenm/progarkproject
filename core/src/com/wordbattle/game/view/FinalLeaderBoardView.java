package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Rectangle;
import com.wordbattle.game.WordBattle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FinalLeaderBoardView {
    private OrthographicCamera cam;
    private Texture background;

    private Texture pinkBubble;
    private Texture goldenBubble;

    private Texture firstPlaceTex;

    private Texture secondPlaceTex;

    private Texture thirdPlaceTex;

    private Texture backToStartTex;

    private Rectangle backToStartBounds;

    private BitmapFont playerFont;

    private BitmapFont titleFont;

    private Map<String, Integer> playerScoresMap;

    private int numBackgroundRenders;

    public FinalLeaderBoardView(OrthographicCamera cam) {
        this.cam = cam;

        backToStartBounds= new Rectangle(30,690, 400,80);

        background= new Texture("bg2.png");
        pinkBubble = new Texture("pink_long-01.png");
        goldenBubble = new Texture("Golden_long-01.png");

        firstPlaceTex=new Texture("Nr1Emoji.png");
        secondPlaceTex = new Texture("Nr2Emoji.png");
        thirdPlaceTex = new Texture("Nr3Emoji.png");

        backToStartTex = new Texture("BackToStartButton.png");

        // Load and set up font
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Knewave-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 34;
        playerFont = generator.generateFont(parameter);
        playerFont.setColor(Color.BLACK);

        parameter.size=70;
        titleFont=generator.generateFont(parameter);
        titleFont.setColor(Color.PINK);
        generator.dispose();
    }

    public void render(SpriteBatch spriteBatch) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();

        for (int i = 0; i < numBackgroundRenders ; i++) {
            spriteBatch.draw(background,0,0-(i*WordBattle.HEIGHT), WordBattle.WIDTH,WordBattle.HEIGHT);
        }
        spriteBatch.draw(backToStartTex, getBackToStartBounds().x,getBackToStartBounds().y-10, getBackToStartBounds().getWidth(), getBackToStartBounds().getHeight()+20);

        titleFont.draw(spriteBatch,"LeaderBoard", 40, 660);

        if (playerScoresMap != null) {
            int yPos = 490; // Initial Y position for the first player entry
            int place = 1; // Initialize the place counter

            // Sort the player scores by score (from high to low)
            List<Map.Entry<String, Integer>> sortedScores = new ArrayList<>(playerScoresMap.entrySet());
            sortedScores.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));

            // Render each player entry
            for (Map.Entry<String, Integer> entry : sortedScores) {
                String playerName = entry.getKey();
                int score = entry.getValue();

                spriteBatch.draw(pinkBubble, 45, yPos, 400, 100); // Adjust X and Y positions as needed
                spriteBatch.draw(getPlaceTexture(place), 80, yPos + 30, 35, 35); // Adjust X and Y positions as needed
                playerFont.draw(spriteBatch, playerName, 120, yPos + 70); // Adjust X and Y positions as needed
                playerFont.draw(spriteBatch, String.valueOf(score), 350, yPos + 70); // Render the score next to the player name

                // Update Y position for the next player entry
                yPos -= 120; // Adjust this value as needed

                // Increment the place counter
                place++;
            }
        }
        spriteBatch.end();
    }

    private Texture getPlaceTexture(int place) {
        switch (place) {
            case 1:
                return firstPlaceTex;
            case 2:
                return secondPlaceTex;
            case 3:
                return thirdPlaceTex;
            default:
                return null;
        }
    }

    public void setMap(Map<String, Integer> playerScores) {
        this.playerScoresMap = playerScores;
        if (!playerScores.isEmpty()) {
            List<Integer> scores = new ArrayList<>(playerScores.values());
            int firstPlayerScore = scores.get(0);
            System.out.println("Score of the first player: " + firstPlayerScore);
        }
    }

    public OrthographicCamera getCam() {
        return cam;
    }

    public Rectangle getBackToStartBounds() {
        return backToStartBounds;
    }

    public void setNumBackgroundRenders(int backgroundRenders){
        this.numBackgroundRenders=backgroundRenders;
    }

    public void dispose(){
        background.dispose();
        pinkBubble.dispose();
        goldenBubble.dispose();
        firstPlaceTex.dispose();
        secondPlaceTex.dispose();
        thirdPlaceTex.dispose();
        backToStartTex.dispose();
    }
}
