package com.wordbattle.game.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;

public class WaitingLobbyView {
    private OrthographicCamera cam;
    private Texture backgroundTexture;

    private Texture waitingTex;

    public WaitingLobbyView(OrthographicCamera cam) {
        this.cam = cam;
        backgroundTexture= new Texture("bg2.png");
        waitingTex = new Texture("WaitingForPlayersToFinish.png");
    }

    public void render(SpriteBatch spriteBatch){

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture,0,0, WordBattle.WIDTH,WordBattle.HEIGHT);
        spriteBatch.draw(waitingTex,5,200,470,270);

        spriteBatch.end();
    }

    public void dispose(){
        backgroundTexture.dispose();
        waitingTex.dispose();
    }
}
