package com.wordbattle.game.network;

import com.wordbattle.game.model.Category;
import com.wordbattle.game.model.Word;

public class FirebaseManager implements FirebaseInterface {


    @Override
    public void SomeFunction() {
        System.out.println("Wubbadubdub");

    }

    @Override
    public void createNewLobby(String hostNickname, String pin, String numRounds) {

    }

    @Override
    public void joinLobby(String pin, String playerNickname, Runnable onSuccess, Runnable onFail) {
    }

    public void createNewCategory(Category category) {

    }

    @Override
    public void addWordToCategory(String categoryName, Word word) {

    }

    @Override
    public void addScoreToDatabase(String lobbyPin, String nickname) {

    }

    @Override
    public void updateScoreInDatabase(String pin, String nickname, int newScore, ScoreUpdateCallback callback) {

    }


    @Override
    public void fetchScores(String pin, ScoreFetchCallback callback) {

    }

    @Override
    public void fetchNumRounds(String lobbyPin, numRoundFetchCallback callback) {

    }


    @Override
    public void fetchPlayers(String pin, PlayerListUpdateCallback callback) {

    }

    @Override
    public void startGame(String pin){

    }

    @Override
    public void isHost(String pin, String nickname, HostCheckCallback callback) {

    }

    @Override
    public void startRound(String pin) {

    }

    @Override
    public void resetRoundFlag(String pin) {

    }


    @Override
    public void listenForGameStart(String pin, GameStartCallback callback) {
    }

    @Override
    public long getSeed() {
        return 0;
    }

    @Override
    public void fetchWord(String pin, String category, WordFetchCallback callback) {

    }

    @Override
    public void checkIfAllPlayersAreFinished(String pin, AllPlayersFinishedCallback callback) {

    }


    @Override
    public void updateFinishedFlag(String nickname, String pin) {

    }

    @Override
    public void getPlayerAndScore(String pin, PlayerScoreCallback callback) {

    }

    @Override
    public void updateRound(String pin, RoundUpdateCallback callback) {

    }

    @Override
    public void fetchRound(String pin, RoundCallback callback) {

    }

    @Override
    public void resetFlags(String pin) {

    }



    @Override
    public void listenForRoundStarted(String pin, Runnable onRoundStarted) {

    }

}

