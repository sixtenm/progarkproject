package com.wordbattle.game.network;

import com.wordbattle.game.model.Category;
import com.wordbattle.game.model.Word;

import java.util.List;
import java.util.Map;

public interface FirebaseInterface {

    public void SomeFunction();


    public void createNewLobby(String hostNickname, String pin, String numRounds);

    public void joinLobby(String pin, String playerNickname, Runnable onSuccess, Runnable onFail);

    void fetchPlayers(String pin, PlayerListUpdateCallback callback);

    void startGame(String pin);


    void isHost(String pin, String nickname, HostCheckCallback callback);

    void startRound(String pin);

    void resetRoundFlag(String pin);

    interface HostCheckCallback {
        void onHostCheckResult(boolean isHost);
        void onError(String error);
    }

    void listenForGameStart(String pin, GameStartCallback callback);

    long getSeed();

    void fetchWord(String pin, String category, WordFetchCallback callback);

    void checkIfAllPlayersAreFinished(String pin, AllPlayersFinishedCallback callback);

    void updateFinishedFlag(String nickname, String pin);

    interface AllPlayersFinishedCallback {
        void onResult(boolean allPlayersFinished);
        void onError(String error);
    }



    void getPlayerAndScore(String pin, PlayerScoreCallback callback);

    void updateRound(String pin, RoundUpdateCallback callback);

    void fetchRound(String pin, RoundCallback callback);

    void resetFlags(String pin);


    void listenForRoundStarted(String pin, Runnable onRoundStarted);

    interface PlayerScoreCallback {
        void onPlayerScoresReceived(Map<String, Integer> playerScores);
    }

    interface RoundUpdateCallback {
        void onRoundUpdateSuccess();
        void onRoundUpdateFailure();
    }


    interface GameStartCallback {
        void onGameStarted();
    }

    // Callback interface
    interface PlayerListUpdateCallback {
        void onPlayerListUpdated(List<String> playerNames);

        void onError(String error);
    }

    interface WordFetchCallback {
        void onWordFetched(String word);
        void onError(String error);
    }

    void createNewCategory(Category category);

    void addWordToCategory(String categoryName, Word word);

    void addScoreToDatabase(String pin, String nickname);

    void updateScoreInDatabase(String pin, String nickname, int newScore, ScoreUpdateCallback callback);
    interface ScoreUpdateCallback {
        void onSuccess();
        void onFailure();
    }

    interface RoundCallback {
        void onRoundFetched(int round);
        void onError();
    }



    void fetchScores(String pin, ScoreFetchCallback callback);

    interface ScoreFetchCallback {
        void onScoresFetched(Map<String, Integer> scores);
        void onError(String error);
    }

    public void fetchNumRounds(String lobbyPin, numRoundFetchCallback callback);

    interface numRoundFetchCallback {
        void onNumRoundFetched(String round);

        void onError(String error);
    }




}

