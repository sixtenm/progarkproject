package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.manager.SoundManager;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.SettingsState;
import com.wordbattle.game.view.SettingsView;

import com.wordbattle.game.manager.MusicManager;

public class SettingsController {
    private SettingsState state;
    private SettingsView settingsView;
    FirebaseInterface _FBIC;

    public SettingsController(SettingsState state, FirebaseInterface _FBIC){
        this.state = state;
        this.settingsView = new SettingsView(state.getCam());
        this._FBIC = _FBIC;
    }

    public void handleInput(){
        if (Gdx.input.justTouched()) {
            SoundManager.playClickSound();
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            System.out.println("Screen touch: " + touchPos.x + ", " + touchPos.y);

            state.getCam().unproject(touchPos);
            System.out.println("World touch: " + touchPos.x + ", " + touchPos.y);

            Rectangle goBackButtonBounds = settingsView.getGoBackButtonBounds();
            Rectangle quitGameButtonBounds = settingsView.getQuitGameButtonBounds();
            Rectangle switchMusicBounds = settingsView.getSwitchMusicBounds();
            Rectangle switchSoundBounds = settingsView.getSwitchSoundBounds();

            if (switchMusicBounds.contains(touchPos.x, touchPos.y)) {
                MusicManager.toggleMusic();
            }

            if (goBackButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Go back button is pressed");
                state.getStateManager().returnToPreviousState();
            }
            if (quitGameButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Quit game");
                state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC));
            }

            if (switchSoundBounds.contains(touchPos.x, touchPos.y)) {
                SoundManager.toggleSound();
            }
        }

    }
    public void update(float dt) {
        handleInput();
    }
    public void render(SpriteBatch sb) {
        settingsView.render(sb);
    }

    public void dispose() {
        settingsView.dispose();
    }

    public void enter() {

    }

    public void exit() {
        dispose();
    }
}