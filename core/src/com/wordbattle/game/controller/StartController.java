package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.states.JoinGameState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.StartState;
import com.wordbattle.game.view.StartView;

public class StartController {
    StartState state;
    StartView startView;

    float timer;

    float loadingTime;



    public StartController(StartState state) {
        this.state = state;
        startView= new StartView(state.getCam());
        timer=0;
        loadingTime= 2;


    }

    public void handleInput(){
        //click anywhere to go to next state, temporary solution until firebase is set up
        if (Gdx.input.justTouched()) {
/*
            state.getStateManager().setState(new MainMenuState(state.getStateManager()));
*/
            System.out.println("clicked on screen");
        }

    }

    public void update(float dt){
        handleInput();
        timer+=dt;
        if (timer>=loadingTime){
            state.getStateManager().setState(new MainMenuState(state.getStateManager(), state.get_FBIC()));
        }


    }

    public void render(SpriteBatch spriteBatch){
        startView.render(spriteBatch);
    }

    public void dispose(){
        startView.dispose();
    }
}
