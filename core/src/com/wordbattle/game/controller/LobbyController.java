package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.model.LobbyModel;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.GamePlayState;
import com.wordbattle.game.states.LobbyState;
import com.wordbattle.game.view.LobbyView;

import java.util.List;

public class LobbyController {
    private LobbyState state;
    private LobbyView lobbyView;
    private String pin;
    private FirebaseInterface _FBIC;
    private String hostNickname;

    private String numRounds;


    public LobbyController(LobbyState state, String hostNickname, String pin,String numRounds, FirebaseInterface _FBIC) {
        this.state = state;
        this.pin = pin;
        this._FBIC = _FBIC;
        this.hostNickname = hostNickname;
        this.numRounds=numRounds;
        this.lobbyView = new LobbyView(state.getCam(), pin);
        _FBIC.createNewLobby(hostNickname, pin, numRounds);
        fetchPlayers();
        //_FBIC.fetchNumRounds();
    }

    private void fetchPlayers() {
        _FBIC.fetchPlayers(pin, new FirebaseInterface.PlayerListUpdateCallback() {
            @Override
            public void onPlayerListUpdated(List<String> playerNames) {
                System.out.println("Fetched players: " + playerNames);
                lobbyView.setPlayerNames(playerNames);
            }
            @Override
            public void onError(String error) {
                System.err.println("Failed to fetch players: " + error);
            }
        });
    }


    public void setInitialScore() {
        _FBIC.addScoreToDatabase(pin, hostNickname);
    }

    public void handleInput() {
        Rectangle startGame = lobbyView.getStartGameButtonBounds();

        if (Gdx.input.justTouched()) {
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos); // convert from screen coordinates to world coordinates

            System.out.println(touchPos);
            System.out.println(startGame.x);
            System.out.println(startGame.y);

            if (startGame.contains(touchPos.x, touchPos.y)) {
                _FBIC.startGame(pin);
                setInitialScore();
                state.getStateManager().setState(new GamePlayState(state.getStateManager(), _FBIC, pin, hostNickname, 3));
            }
        }
    }

    public void update(float dt) {
        handleInput();
    }

    public void dispose() {
        // Clean up any resources when exiting the state
    }

    public void render(SpriteBatch sb) {
        lobbyView.render(sb);
    }
}
