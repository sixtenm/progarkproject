package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.manager.SoundManager;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.LobbyState;
import com.wordbattle.game.states.CreateGameState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.SettingsState;
import com.wordbattle.game.view.CreateGameView;
import com.wordbattle.game.manager.SoundManager;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.SettingsState;
import com.badlogic.gdx.math.Rectangle;


public class CreateGameController {
    private CreateGameState state;
    private CreateGameView createGameView;
    private String selectedLevel;
    private FirebaseInterface _FBIC;

    public CreateGameController(CreateGameState state, FirebaseInterface _FBIC) {
        this.state = state;
        this._FBIC = _FBIC;
        this.createGameView = new CreateGameView(state.getCam()); // Assuming you provide a getter for the camera in BaseState

    }
        public void handleInput() {
            if (Gdx.input.justTouched()) {
                SoundManager.playClickSound();
                Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                state.getCam().unproject(touchPos); // convert from screen coordinates to world coordinates
                Rectangle settingsBounds = createGameView.getSettingsBounds();

                // Assuming createGameView exposes the bounds via getters
                if (createGameView.getEasyButtonBounds().contains(touchPos.x, touchPos.y)) {
                    selectedLevel = "easy";
                    System.out.println(createGameView.returnBound());
                    createGameView.setSelectedLevel(selectedLevel);
                } else if (createGameView.getMediumButtonBounds().contains(touchPos.x, touchPos.y)) {
                    selectedLevel = "medium";
                    createGameView.setSelectedLevel(selectedLevel);
                } else if (createGameView.getHardButtonBounds().contains(touchPos.x, touchPos.y)) {
                    selectedLevel = "hard";
                    createGameView.setSelectedLevel(selectedLevel);
                }
                
                if (createGameView.getCreateGameBounds().contains(touchPos.x, touchPos.y)) {
                    if (confirmNickname()) {
                        state.getStateManager().setState(new LobbyState(state.getStateManager(), createGameView.getPin(), createGameView.getNickname(),createGameView.getNumRounds(), _FBIC));
                    }
                    else{
                        createGameView.setNickNameMessage("Nickname is empty");

                    }
                }
                if (settingsBounds.contains(touchPos.x, touchPos.y)) {
                    System.out.println("Settings Pressed");
                    state.getStateManager().setState(new SettingsState(state.getStateManager(), _FBIC));
                }

                if (createGameView.getGoBackButtonBounds().contains(touchPos.x, touchPos.y)) {
                    state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC ));
                }

                if (createGameView.getGoBackButtonBounds().contains(touchPos.x, touchPos.y)) {
                    state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC ));
                }

                if(createGameView.getRound3ButtonBounds().contains(touchPos.x, touchPos.y)){
                    createGameView.setNumRounds("3");
                }
                if(createGameView.getRound5ButtonBounds().contains(touchPos.x, touchPos.y)){
                    createGameView.setNumRounds("5");
                }
                if(createGameView.getRound10ButtonBounds().contains(touchPos.x, touchPos.y)){
                    createGameView.setNumRounds("10");
                }
            }
        }

    public boolean confirmNickname(){
        String Nickname = createGameView.getNickname();
        //does not allow empty string or only whitespace
        return !Nickname.trim().isEmpty();
    }

    public void update(float dt) {
        handleInput();

    }

    public void render(SpriteBatch sb) {
        createGameView.render(sb);
    }

    public void dispose() {
        createGameView.dispose();
    }

    public void enter() {

    }

    public void exit() {
        dispose();
    }
}