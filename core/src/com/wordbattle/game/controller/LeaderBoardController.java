package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.GamePlayState;
import com.wordbattle.game.states.LeaderBoardState;
import com.wordbattle.game.view.LeaderBoardView;

public class LeaderBoardController {
    private LeaderBoardState state;
    private LeaderBoardView leaderBoardView;

    private int lenLeaderBordPage;
    private FirebaseInterface _FBIC;
    private String pin;
    private String nickname;

    private int numRounds;


    public LeaderBoardController(LeaderBoardState state, FirebaseInterface _FBIC, String pin, String nickname, int numRounds) {
        this.state = state;
        this._FBIC = _FBIC;
        this.pin = pin;
        this.nickname = nickname;
        this.numRounds=numRounds;

        resetFinishedFlagForAllPlayers();
        leaderBoardView = new LeaderBoardView(state.getCam());
        lenLeaderBordPage=2400; //testing variable until firebase is implemented
        leaderBoardView.setNumBackgroundRenders(calculateBackgroundRenders()); //Tells view how many backgroundTextures to load
        getPlayerAndScore();
        listenForRoundStarted();
        checkHost();
    }

    private void listenForRoundStarted() {
        // Assuming _FBIC is your FirebaseInterface instance and pin is your game's pin
        _FBIC.listenForRoundStarted(pin, () -> {
            // This code will be executed when the round starts flag is set to true
            Gdx.app.postRunnable(this::startNewRound);
        });
    }


    private void checkHost() {
        _FBIC.isHost(pin, nickname, new FirebaseInterface.HostCheckCallback() {
            @Override
            public void onHostCheckResult(boolean isHost) {
                if (isHost) {
                    leaderBoardView.setHost(true);
                } else {
                    leaderBoardView.setHost(false);
                }
            }

            @Override
            public void onError(String error) {
                // Handle error
            }
        });
    }


    private void startNewRound() {
        System.out.println("A new round has started. Preparing game for the next round...");
        state.getStateManager().setState(new GamePlayState(state.getStateManager(), _FBIC, pin, nickname, numRounds));
        // This is not working
    }

    private void resetFinishedFlagForAllPlayers() {
        _FBIC.resetFlags(pin);

    }

    int mouseYCoordinate;

    public void handleInput(){
        if (Gdx.input.justTouched()){
            mouseYCoordinate =Gdx.input.getY();
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos); // convert from screen coordinates to world coordinates
            if(leaderBoardView.isHost()) {
                if (leaderBoardView.getNextRoundBounds().contains(touchPos.x, touchPos.y)) {
                    _FBIC.startRound(pin); // Stasrt the round
                    state.getStateManager().setState(new GamePlayState(state.getStateManager(), _FBIC, pin, nickname, numRounds));
                }
            }
        }
        if (Gdx.input.isTouched()){
            int scrollDistance=mouseYCoordinate-Gdx.input.getY();

            mouseYCoordinate=Gdx.input.getY();
            if( (leaderBoardView.getCam().viewportHeight/2 + leaderBoardView.getCam().position.y) -scrollDistance  < WordBattle.HEIGHT && ( (-leaderBoardView.getCam().viewportHeight/2) + leaderBoardView.getCam().position.y) -scrollDistance > WordBattle.HEIGHT-lenLeaderBordPage ){
                leaderBoardView.getCam().translate(0,-scrollDistance);
            }
        }
    }


    public void getPlayerAndScore() {
        _FBIC.getPlayerAndScore(pin, playerScores -> {
            Gdx.app.postRunnable(() -> {
                if (leaderBoardView != null) {
                    leaderBoardView.setMap(playerScores);
                    System.out.println(playerScores);
                }
            });
        });
    }

    public void render(SpriteBatch sb){
        leaderBoardView.render(sb);
    }

    public void update(float dt){
        handleInput();
    }

    private int calculateBackgroundRenders(){
        return 1+ lenLeaderBordPage / WordBattle.HEIGHT;
    }

    public void dispose(){
        leaderBoardView.dispose();
    }
}
