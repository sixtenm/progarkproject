package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.CreateGameState;
import com.wordbattle.game.states.GamePlayState;
import com.wordbattle.game.states.JoinGameState;
import com.wordbattle.game.states.WaitForHostState;
import com.wordbattle.game.view.JoinGameView;
import com.wordbattle.game.view.WaitForHostView;

import java.util.List;


public class WaitForHostController {
    private WaitForHostState state;
    private WaitForHostView waitForHostView;
    private JoinGameView joinGameView;

    private int lenWaitingPage;
    private FirebaseInterface _FBIC;
    private String nickname;
    private String pin;

    private int numRounds;

    public WaitForHostController(WaitForHostState state, FirebaseInterface _FBIC, String nickname, String pin, int  numRounds ) { // This takes in pin and nickname from inout field
        this.state = state;
        this.nickname = nickname;
        this.pin = pin;
        this._FBIC = _FBIC;
        this.waitForHostView = new WaitForHostView(state.getCam(), _FBIC, nickname, pin);
        this.numRounds=numRounds;
        fetchPlayers();
        listenForStart();
        _FBIC.addScoreToDatabase(pin, nickname);
    }

    private void fetchPlayers() {
        _FBIC.fetchPlayers(pin, new FirebaseInterface.PlayerListUpdateCallback() {
            @Override
            public void onPlayerListUpdated(List<String> playerNames) {
                System.out.println("Fetched players: " + playerNames);
                waitForHostView.setPlayerNames(playerNames);
            }
            @Override
            public void onError(String error) {
                System.err.println("Failed to fetch players: " + error);
            }
        });
    }

    private void listenForStart() {
        // Inside your listener method
        _FBIC.listenForGameStart(pin, () -> {
            Gdx.app.postRunnable(() -> {
                System.out.println("Gameflag start triggered");
                state.getStateManager().setState(new GamePlayState(state.getStateManager(), _FBIC, pin, nickname, numRounds));
            });
        });

    }

    private void handleInput(){

    }

    public void update(float dt){
        handleInput();
    }

    public void render(SpriteBatch spriteBatch) {
            waitForHostView.render(spriteBatch);
    }

    public void dispose(){
        waitForHostView.dispose();
    }
}
