/*
package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.states.ConnectingLobbyState;
import com.wordbattle.game.states.StartingGameLoadingState;
import com.wordbattle.game.view.LobbyView;

public class ConnectingLobbyController {
    private ConnectingLobbyState state;
    private LobbyView lobbyView;

    private String pin;


    public ConnectingLobbyController(ConnectingLobbyState state) {
        this.state = state;
        this.lobbyView = new LobbyView(state.getCam(),state.getPin());
    }

    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos); // convert from screen coordinates to world coordinates

            if (lobbyView.getCreateGameBounds().contains(touchPos.x, touchPos.y)) {
                state.getStateManager().setState(new StartingGameLoadingState(state.getStateManager()));

            }
        }
    }
    public void update(float dt) {
        handleInput();
    }

    public void dispose() {
        lobbyView.dispose();
    }
}
*/
