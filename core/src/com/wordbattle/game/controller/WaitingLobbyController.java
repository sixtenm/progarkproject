package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.FinalLeaderboardState;
import com.wordbattle.game.states.LeaderBoardState;
import com.wordbattle.game.states.WaitingLobbyState;
import com.wordbattle.game.view.WaitingLobbyView;

public class WaitingLobbyController {
    private WaitingLobbyState state;
    private WaitingLobbyView waitingLobbyView;
    private String nickname;
    private int score;
    private FirebaseInterface _FBIC;
    private String pin;
    private float timer;
    private boolean allPlayersFinished;
    private boolean scoreUpdated = false; // Add this field
    private boolean roundUpdated = false;
    private int round;
    private int numRounds;
    private boolean isHost;


    public WaitingLobbyController(WaitingLobbyState state, FirebaseInterface _FBIC, String nickname, int score, String pin, int numRounds) {
        this.state = state;
        this._FBIC = _FBIC;
        this.nickname = nickname;
        this.score = score;
        this.pin = pin;
        this.numRounds=numRounds;

        checkHost();
        updateScore();
        updateRound(() -> {
            roundUpdated = true;
        });
        fetchRound();

        System.out.println("number of rounds" + numRounds);

        waitingLobbyView= new WaitingLobbyView(state.getCam());
    }

    private void checkHost() {
        _FBIC.isHost(pin, nickname, new FirebaseInterface.HostCheckCallback() {
            @Override
            public void onHostCheckResult(boolean isHost) {
                setHost(isHost);
            }

            @Override
            public void onError(String error) {
                // Handle error
            }
        });
    }

    public void setHost(boolean host) {
        this.isHost = host;
    }

    public void updateRound(Runnable onRoundUpdated) {
        if (isHost) {
            _FBIC.updateRound(pin, new FirebaseInterface.RoundUpdateCallback() {
                @Override
                public void onRoundUpdateSuccess() {
                    Gdx.app.postRunnable(onRoundUpdated);
                }

                @Override
                public void onRoundUpdateFailure() {
                    System.out.println("Failed to update round");
                }
            });
        }
    }

    public void handleInput(){

    }
    public void fetchRound() {
        _FBIC.fetchRound(pin, new FirebaseInterface.RoundCallback() {
            @Override
            public void onRoundFetched(int fetchedRound) {
                Gdx.app.postRunnable(() -> {
                    round = fetchedRound;
                });
            }

            @Override
            public void onError() {
                System.err.println("Error fetching round");
            }
        });
    }


    public void updateScore() {
        _FBIC.updateScoreInDatabase(pin, nickname, score, new FirebaseInterface.ScoreUpdateCallback() {
            @Override
            public void onSuccess() {
                System.out.println("Score updated successfully.");
                Gdx.app.postRunnable(() -> {
                    scoreUpdated = true;
                });
            }

            @Override
            public void onFailure() {
                System.out.println("Score update failed.");
            }
        });
    }

    public void checkFinishedFlag() {
        _FBIC.checkIfAllPlayersAreFinished(pin, new FirebaseInterface.AllPlayersFinishedCallback() {
            @Override
            public void onResult(boolean allPlayersFinishedCheck) {
                if (allPlayersFinishedCheck) {
                    allPlayersFinished = true;
                }
            }

            @Override
            public void onError(String error) {
                // Handle the error
            }
        });
    }


    public void update(float dt) {
        timer += dt;
        checkFinishedFlag();
        if (scoreUpdated) {
            if (round < numRounds-1 && allPlayersFinished) { // This means 4 rounds, starting from 0
                Gdx.app.postRunnable(() -> {
                    state.getStateManager().setState(new LeaderBoardState(state.getStateManager(), _FBIC, pin, nickname, numRounds));
                });
            } else if (round >= numRounds-1) {
                Gdx.app.postRunnable(() -> {
                    state.getStateManager().setState(new FinalLeaderboardState(state.getStateManager(), _FBIC, pin, nickname));
                });
            }
        }
            handleInput();
    }

    public void render(SpriteBatch spriteBatch){
        waitingLobbyView.render(spriteBatch);
    }

    public void dispose(){
        waitingLobbyView.dispose();
    }
}
