package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.GamePlayState;
import com.wordbattle.game.states.SettingsState;
import com.wordbattle.game.states.WaitingLobbyState;
import com.wordbattle.game.view.CreateGameView;
import com.wordbattle.game.view.GamePlayView;

import java.util.Arrays;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

public class GamePlayController {
    private final GamePlayState state;
    private final GamePlayView gamePlayView;
    private FirebaseInterface _FBIC;
    private String pin;
    private String word;
    private Character selectedLetter = null;
    private int selectedIndex = -1;
    private String nickname;
    private int round;
    private int numRounds;
    private String dummy;


    public GamePlayController(GamePlayState state, FirebaseInterface _FBIC, String pin, String nickname, int numRounds){
        this.state = state;
        this._FBIC = _FBIC;
        this.pin = pin;
        this.nickname = nickname;
        this.gamePlayView = new GamePlayView(state.getCam(), _FBIC, pin); // Assuming you provide a getter for the camera in BaseState
        this.numRounds= numRounds;
        System.out.println("number of rounds" + numRounds);

        resetRoundFLag();

        // Fetch the word using the PIN and the category
        fetchWord();
        fetchNumRounds();

        System.out.println("numRounds" + numRounds);
        System.out.println("dummy is"+dummy);
    }


    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos); // Adjusts the touch position to the game's coordinate system.
            System.out.println("Clicked on the screen");
            Rectangle settingsBounds = gamePlayView.getSettingsBounds();
            if (settingsBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Settings Pressed");
                state.getStateManager().setState(new SettingsState(state.getStateManager(), _FBIC));
            }
            for (int i = 0; i < gamePlayView.getLetterBounds().length; i++) {
                if (gamePlayView.getLetterBounds()[i].contains(touchPos.x, touchPos.y)) {
                    // The i-th letter was clicked
                    // Handle the click, like selecting or deselecting a letter
                    selectedLetter = gamePlayView.getLetters().get(i);
                    System.out.println("Clicked on letter " + gamePlayView.getLetters().get(i));
                    selectedIndex = i;

                // Try placing the selected letter in the next available box
                    boolean letterPlaced = false;
                    for (int j = 0; j < gamePlayView.getFilledBoxes().length; j++) {
                        if (gamePlayView.getFilledBoxes()[j] == null) { // Check if the box is empty
                            // Get the current filledBoxes
                            System.out.println("Before setting letter: " + Arrays.toString(gamePlayView.getFilledBoxes()));

                            Character[] currentFilledBoxes = gamePlayView.getFilledBoxes();
                            System.out.println(currentFilledBoxes.length);

                            // Place the selected letter in the j-th box
                            currentFilledBoxes[j] = selectedLetter;

                            // Update the filledBoxes in the view
                            gamePlayView.setFilledBoxes(currentFilledBoxes);
                            gamePlayView.getLetters().remove(i);
                            checkWordCompletion();



                            System.out.println("Letter " + selectedLetter + " placed in box " + j);
                            System.out.println("After setting letter: " + Arrays.toString(gamePlayView.getFilledBoxes()));


                            letterPlaced = true;
                            break; // Exit the loop since we've placed the letter
                        }
                    }

                if (!letterPlaced) {
                    // All boxes are filled, handle accordingly.
                    System.out.println("All boxes are filled.");
                }

                break; // Break from the loop checking letter bounds
            }
        }
        for (int i = 0; i < gamePlayView.getLetterBoxBounds().length; i++) {
            if (gamePlayView.getLetterBoxBounds()[i].contains(touchPos.x, touchPos.y)) {
                Character letterToRemove = gamePlayView.getFilledBoxes()[i];
                gamePlayView.getFilledBoxes()[i] = null; // Clear the letter from the box
                gamePlayView.getLetters().add(selectedIndex, letterToRemove);
                System.out.println("Removed letter " + letterToRemove + " from box " + i);
                break; // Exit the loop since we've handled the box click
            }
        }

        }

    }


    private void resetRoundFLag() {
        _FBIC.resetRoundFlag(pin);
    }

    private void fetchNumRounds(){
        System.out.println("outer layer");
        _FBIC.fetchNumRounds(pin, new FirebaseInterface.numRoundFetchCallback(){

            @Override
            public void onNumRoundFetched(String numRoundDb) {

                Gdx.app.postRunnable(() -> {

                    numRounds=Integer.parseInt(numRoundDb);
                });
            }

            @Override
            public void onError(String error) {
            }
        });

    }


    public void fetchWord() {
        _FBIC.fetchRound(pin, new FirebaseInterface.RoundCallback() {
            @Override
            public void onRoundFetched(int fetchedRound) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        round = fetchedRound;
                        // Now that we have the round, you can perform checks or update the state
                    }
                });
            }

            @Override
            public void onError() {
                System.err.println("Error fetching round");
                // Handle error, perhaps retry or provide user feedback
            }
        });
        _FBIC.fetchWord(pin + round, "Animals", new FirebaseInterface.WordFetchCallback() {
            @Override
            public void onWordFetched(String word) {
                gamePlayView.setCurrentWord(word.toUpperCase());
                System.out.println("Set word to " + word);
            }

            @Override
            public void onError(String error) {
                // Handle the error here
                System.err.println("Error fetching word: " + error);
            }
        });
    }


    private void checkWordCompletion() {
        Character[] filledBoxes = gamePlayView.getFilledBoxes();
        StringBuilder userWord = new StringBuilder(filledBoxes.length);
        boolean isComplete = true;

        // Build the word from filled boxes and check if any box is still empty
        for (Character letter : filledBoxes) {
            if (letter == null) {
                isComplete = false; // Found an empty box, so the word isn't complete
                break;
            }
            userWord.append(letter);
        }

        if (isComplete) {
            if (userWord.toString().equals(gamePlayView.getCurrentWord())) {
                gamePlayView.stopTimer();

                System.out.println("Congratulations! The word is correct: " + userWord);
                _FBIC.updateFinishedFlag(nickname, pin);
                System.out.println("Score: " + calculateScore(gamePlayView.getTimeLeft(), gamePlayView.getMaxTime()));
                state.getStateManager().setState(new WaitingLobbyState(state.getStateManager(), _FBIC, nickname, calculateScore(gamePlayView.getTimeLeft(), gamePlayView.getMaxTime()), pin, numRounds));
                // Here you can handle the event when the user correctly completes the word
                // For example, proceed to the next word, update score, etc.

            } else {
                System.out.println("The word is incorrect: " + userWord + ". Try again!");
                // Here you can handle the event when the user fills all boxes but the word is incorrect
                // For example, clear the boxes for another attempt, show an error message, etc.
            }
        }
    }

    public void update(float dt) {
        gamePlayView.update(dt);
        handleInput();
        //Check if gameTimer is up. If so allert database, award player 0 points and go to waitingLobby.
        if (gamePlayView.getTimeLeft()<=0.0){
            _FBIC.updateFinishedFlag(nickname, pin);
            state.getStateManager().setState(new WaitingLobbyState(state.getStateManager(), _FBIC, nickname, 0, pin, numRounds));
        }
    }

    public int calculateScore(float timeLeft, float maxTime) {
        // Parameters to shape the S-curve
        float steepness = 10.0f; // Change this to make the curve steeper or shallower
        float midpoint = maxTime / 2.0f; // Midpoint of the time where the curve will be centered

        // Convert timeLeft into a score between 0 and 1 using the sigmoid function
        double scoreFraction = 1 / (1 + Math.exp(-steepness * ((timeLeft / maxTime) - midpoint / maxTime)));

        // Convert the fraction into an actual score value, let's say the maximum score is 1000
        int maxScore = 1000;
        int score = (int)(scoreFraction * maxScore);

        return score;
    }

    public void render(SpriteBatch sb) {gamePlayView.render(sb);}

    public void dispose() {gamePlayView.dispose();}

    public void enter() {

    }

    public void exit() {

        dispose();
    }
}


