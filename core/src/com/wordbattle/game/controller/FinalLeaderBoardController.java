package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.CreateGameState;
import com.wordbattle.game.states.FinalLeaderboardState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.view.FinalLeaderBoardView;

public class FinalLeaderBoardController {
    private FinalLeaderboardState state;

    private FinalLeaderBoardView finalLeaderBoardView;

    private int lenLeaderBordPage;
    private String pin;
    private FirebaseInterface _FBIC;
    private String nickname;


    public FinalLeaderBoardController(FinalLeaderboardState state, FirebaseInterface _FBIC, String pin, String nickname) {
        this.state=state;
        this._FBIC = _FBIC;
        this.pin = pin;
        this.nickname = nickname;
        finalLeaderBoardView = new FinalLeaderBoardView(state.getCam());
        lenLeaderBordPage=2400; //testing variable until firebase is implemented
        finalLeaderBoardView.setNumBackgroundRenders(calculateBackgroundRenders()); //Tells view how many backgroundTextures to load
        getPlayerAndScore();
    }

    int mouseYCoordinate;

    public void handleInput(){
        if (Gdx.input.justTouched()){
            mouseYCoordinate =Gdx.input.getY();
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos); // convert from screen coordinates to world coordinates
            if (finalLeaderBoardView.getBackToStartBounds().contains(touchPos.x, touchPos.y)){
                state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC)); //midlertidlig
            }
        }
        if (Gdx.input.isTouched()){
            int scrollDistance=mouseYCoordinate-Gdx.input.getY();
            mouseYCoordinate=Gdx.input.getY();
            if( (finalLeaderBoardView.getCam().viewportHeight/2 + finalLeaderBoardView.getCam().position.y) -scrollDistance  < WordBattle.HEIGHT && ( (-finalLeaderBoardView.getCam().viewportHeight/2) + finalLeaderBoardView.getCam().position.y) -scrollDistance > WordBattle.HEIGHT-lenLeaderBordPage ){
                finalLeaderBoardView.getCam().translate(0,-scrollDistance);
            }
        }
    }

    public void getPlayerAndScore() {
        _FBIC.getPlayerAndScore(pin, playerScores -> {
            Gdx.app.postRunnable(() -> {
                if (finalLeaderBoardView != null) {
                    finalLeaderBoardView.setMap(playerScores);
                    System.out.println(playerScores);
                }
            });
        });
    }

    public void render(SpriteBatch sb){
        finalLeaderBoardView.render(sb);
    }

    public void update(float dt){
        handleInput();
    }

    private int calculateBackgroundRenders(){
        return 1 +lenLeaderBordPage / WordBattle.HEIGHT;
    }

    public void dispose(){
        finalLeaderBoardView.dispose();
    }
}
