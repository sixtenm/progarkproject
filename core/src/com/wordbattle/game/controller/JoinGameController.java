package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.JoinGameState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.SettingsState;
import com.wordbattle.game.states.WaitForHostState;
import com.wordbattle.game.view.JoinGameView;

import java.util.List;

public class JoinGameController {

    private JoinGameState state;
    private JoinGameView joinGameView;

    private FirebaseInterface _FBIC;
    private String errorMsg;

    private boolean isLessThan6Members;

    private String toManyPlayerMessage;

    public JoinGameController(JoinGameState state, FirebaseInterface _FBIC, String errorMsg) {
        this.state = state;
        this._FBIC = _FBIC;
        this.errorMsg = errorMsg;
        this.joinGameView = new JoinGameView(state.getCam());
    }

    public void handleInput() {

        if (Gdx.input.justTouched()) {
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos);
            Rectangle joinGameButtonBounds = joinGameView.getJoinGameButtonBounds();
            Rectangle settingsBounds = joinGameView.getSettingsBounds();
            if (joinGameButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Join Game Button Pressed in joinGameController");
                String nickname = joinGameView.getNickname();
                String pin = joinGameView.getPin();
                if (confirmNickname() && confirmPin() && confirmLessThan6()) { // This is just to check for spelling
                    _FBIC.joinLobby(pin, nickname, () -> {
                        // Navigate to WaitForHostState if joining lobby was successful
                        Gdx.app.postRunnable(() -> state.getStateManager().setState(new WaitForHostState(state.getStateManager(), _FBIC, nickname, pin, 3)));
                    }, () -> {
                        // Stay on JoinGameState and display an error message if joining lobby failed
                        Gdx.app.postRunnable(() -> joinGameView.setErrorMessage("Lobby does not exist"));
                    });
                } else {
                   //todo
                }
            }
            if (settingsBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Settings Pressed");
                state.getStateManager().setState(new SettingsState(state.getStateManager(), _FBIC));
            }
            if (joinGameView.getGoBackButtonBounds().contains(touchPos.x, touchPos.y)) {
                state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC ));
            }
        }
    };

    public void update(float dt) {
        handleInput();
    }


    public void render(SpriteBatch sb){
        joinGameView.render(sb);
    }

    public boolean confirmPin(){
        if (true){
            return true;
        }
        else{
            joinGameView.setErrorMessage("Invalid Pin");
            return false;
        }
    }

    public boolean confirmNickname(){
        String Nickname = joinGameView.getNickname();
        if (Nickname.trim().isEmpty()) { //does not allow empty string or only whitespace
            joinGameView.setNicknameMessage("Nickname must be filled out");
            return false;
        }
        else{
            return true;
        }
    }

    public boolean confirmLessThan6(){

        _FBIC.fetchPlayers(joinGameView.getPin(), new FirebaseInterface.PlayerListUpdateCallback() {
            @Override
            public void onPlayerListUpdated(List<String> playerNames) {
                if (playerNames.size()<6){
                    System.out.println("checked how many players");
                    isLessThan6Members=true;
                }
                else{
                    isLessThan6Members =false;
                    joinGameView.setLobbyIsFullMessage("Lobby is full");
                }
            }

            @Override
            public void onError(String error) {
            }
        });
        return isLessThan6Members;
    }

    public void dispose() {
        joinGameView.dispose();
    }
}
