package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.HowToPlayState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.view.HowToPlayView;

public class HowToPlayController {
    private HowToPlayState state;
    private HowToPlayView howToPlayView;
    private FirebaseInterface _FBIC;

    public HowToPlayController(HowToPlayState state, FirebaseInterface _FBIC) {
        this.state = state;
        this._FBIC = _FBIC;
        this.howToPlayView = new HowToPlayView(state.getCam(), _FBIC);
    }

    public void handleInput() {
        if (Gdx.input.justTouched()) {
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            state.getCam().unproject(touchPos);
            if (howToPlayView.getInvisibleButtonHintBounds().contains(touchPos.x, touchPos.y)) {
                howToPlayView.toggleShowHint();
            }
            if (howToPlayView.getInvisibleButtonGoBackBounds().contains(touchPos.x, touchPos.y)) {
                state.getStateManager().setState(new MainMenuState(state.getStateManager(), _FBIC ));
            }
        }
    }

    public void update(float dt) {
        handleInput();
    }

    public void render(SpriteBatch sb) {
        howToPlayView.render(sb);
    }

    public void dispose() {
        howToPlayView.dispose();
    }
}
