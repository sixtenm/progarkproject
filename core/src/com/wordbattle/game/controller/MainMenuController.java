package com.wordbattle.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.wordbattle.game.model.CreateWords;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.CreateGameState;
import com.wordbattle.game.states.HowToPlayState;
import com.wordbattle.game.states.JoinGameState;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.WaitForHostState;
import com.wordbattle.game.view.MainMenuView;
import com.wordbattle.game.states.SettingsState;
import com.badlogic.gdx.audio.Sound;
import com.wordbattle.game.manager.SoundManager;


public class MainMenuController {
    private Sound clickSound;
    private MainMenuState state;
    private MainMenuView mainMenuView;
    FirebaseInterface _FBIC;

    public MainMenuController(MainMenuState state, FirebaseInterface _FBIC) {
        this.state = state;
        clickSound = Gdx.audio.newSound(Gdx.files.internal("click.wav"));
        // This line initializes the view for the page
        this.mainMenuView = new MainMenuView(state.getCam()); // Assuming you provide a getter for the camera in BaseState
        this._FBIC = _FBIC;
    }

    public void handleInput() {

        if (Gdx.input.justTouched()) {
            SoundManager.playClickSound();
            Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            System.out.println("Screen touch: " + touchPos.x + ", " + touchPos.y);

            state.getCam().unproject(touchPos); // Transform touch coordinates
            System.out.println("World touch: " + touchPos.x + ", " + touchPos.y);

            Rectangle joinGameButtonBounds = mainMenuView.getJoinGameButtonBounds();
            Rectangle newGameButtonBounds = mainMenuView.getNewGameButtonBounds();
            Rectangle howToPlayButtonBounds = mainMenuView.getHowToPlayButtonBounds();
            Rectangle settingsBounds = mainMenuView.getSettingsBounds();

            // Debugging button bounds
            System.out.println("Join Game Button Bounds: " + joinGameButtonBounds.x +"," + joinGameButtonBounds.y);
            System.out.println("New Game Button Bounds: " + newGameButtonBounds);
            System.out.println("How to play Button Bounds: " + howToPlayButtonBounds);
            System.out.println("How to play Button Bounds: " + howToPlayButtonBounds);

            // Button checks
            if (joinGameButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Join Game Button Pressed, takning you to WaitForHost");
                state.getStateManager().setState(new JoinGameState(state.getStateManager(), _FBIC, ""));
            }
            if (newGameButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("New Game Button Pressed");
                state.getStateManager().setState(new CreateGameState(state.getStateManager(), _FBIC));
            }
            if (howToPlayButtonBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("How to play Pressed");
                state.getStateManager().setState(new HowToPlayState(state.getStateManager(), _FBIC));
            }
            if (settingsBounds.contains(touchPos.x, touchPos.y)) {
                System.out.println("Settings Pressed");
                state.getStateManager().setState(new SettingsState(state.getStateManager(), _FBIC));
            }
        }
    }


    public void update(float dt) {
        handleInput();
    }

    public void render(SpriteBatch sb) {
        mainMenuView.render(sb);
    }

    public void dispose() {
        mainMenuView.dispose();
    }

    public void enter() {
    }

    public void exit() {
        dispose();
    }
}
