package com.wordbattle.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundManager {
    private static Sound clickSound;
    private static boolean isSoundOn = true;

    public static void initialize() {
        clickSound = Gdx.audio.newSound(Gdx.files.internal("click.wav"));
    }

    public static void playClickSound() {
        if (isSoundOn) {
            clickSound.play();
        }
    }

    public static void toggleSound() {
        isSoundOn = !isSoundOn;
    }

    public static boolean isSoundOn() {
        return isSoundOn;
    }

    public static void dispose() {
        clickSound.dispose();
    }
}