package com.wordbattle.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicManager {
    private static Music backgroundMusic;
    private static boolean isMusicOn = true;


    public static void initialize() {
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("bgMusic.mp3"));
        backgroundMusic.setLooping(true);
    }

    public static void playMusic() {
        if (backgroundMusic != null && !backgroundMusic.isPlaying() && isMusicOn) {
            backgroundMusic.play();
        }
    }

    public static void stopMusic() {
        if (backgroundMusic != null && backgroundMusic.isPlaying()) {
            backgroundMusic.stop();
        }
    }

    public static void toggleMusic() {
        isMusicOn = !isMusicOn;
        if (isMusicOn) {
            playMusic();
        } else {
            stopMusic();
        }
    }

    public static void dispose() {
        if (backgroundMusic != null) {
            backgroundMusic.dispose();
        }
    }

    public static boolean isMusicOn() {
        return isMusicOn;
    }
}