package com.wordbattle.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.wordbattle.game.model.CreateWords;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.states.MainMenuState;
import com.wordbattle.game.states.StartState;
import com.wordbattle.game.states.StateManager;
import com.wordbattle.game.manager.SoundManager;
import com.wordbattle.game.manager.MusicManager;

import com.wordbattle.game.manager.MusicManager;

public class WordBattle extends ApplicationAdapter {
	public static final int WIDTH = 480;
	public static final int HEIGHT = 800;
	public static final String TITLE = "WordBattle";
	private StateManager stateManager;
	public SpriteBatch batch;
	FirebaseInterface _FBIC;

	public WordBattle(FirebaseInterface FBIC) {_FBIC = FBIC;}

	@Override
	public void create() {
		_FBIC.SomeFunction();
		batch = new SpriteBatch();
		stateManager = StateManager.getInstance();
		ScreenUtils.clear(1, 0, 0, 1);
		stateManager.setState(new StartState(stateManager,_FBIC)); // Set the main menu as the initial state
		MusicManager.initialize();
		MusicManager.playMusic();
		SoundManager.initialize();
	}
	@Override
	public void render() {
		stateManager.update(Gdx.graphics.getDeltaTime());
		stateManager.render(batch);
	}

	@Override
	public void dispose() {
		stateManager.dispose();
		batch.dispose();

	}
}