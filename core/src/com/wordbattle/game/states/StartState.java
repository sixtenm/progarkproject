package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.MainMenuController;
import com.wordbattle.game.controller.StartController;
import com.wordbattle.game.network.FirebaseInterface;

public class StartState extends BaseState {
    StartController controller;

    FirebaseInterface _FBIC;


    public StartState(StateManager gsm, FirebaseInterface _FBIC) {
        super(gsm);
        this.controller = new StartController(this); // 'this' provides context
        this._FBIC=_FBIC;
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);

    }

    public StateManager getStateManager(){
        return gsm;
    }

    public FirebaseInterface get_FBIC() {
        return _FBIC;
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {

    }
}
