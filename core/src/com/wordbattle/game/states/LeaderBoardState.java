package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.LeaderBoardController;
import com.wordbattle.game.network.FirebaseInterface;

public class LeaderBoardState extends BaseState {
    private FirebaseInterface _FBIC;
    private String pin;
    private String nickname;

    LeaderBoardController controller;
    public LeaderBoardState(StateManager gsm, FirebaseInterface _FBIC, String pin, String nickname, int numRounds) {
        super(gsm);
        this._FBIC = _FBIC;
        this.pin = pin;
        this.nickname = nickname;
        this.controller = new LeaderBoardController(this, _FBIC, pin, nickname,numRounds); // 'this' provides context
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    public StateManager getStateManager() {
        return gsm;
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {
        controller.dispose();
    }
}
