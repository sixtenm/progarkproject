package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.FinalLeaderBoardController;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.network.FirebaseManager;

public class FinalLeaderboardState extends BaseState {

    private String pin;
    private FirebaseInterface _FBIC;
    private String nickname;

    FinalLeaderBoardController controller;
    public FinalLeaderboardState(StateManager gsm, FirebaseInterface _FBIC, String pin, String nickname) {
        super(gsm);
        this._FBIC = _FBIC;
        this.pin = pin;
        this.nickname = nickname;
        this.controller = new FinalLeaderBoardController(this, _FBIC, pin, nickname); // 'this' provides context
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {
    }

    @Override
    public void exit() {
    }

    public StateManager getStateManager() {
        return gsm;
    }

    @Override
    public void dispose() {
        controller.dispose();
    }
}
