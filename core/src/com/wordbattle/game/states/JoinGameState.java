package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.JoinGameController;
import com.wordbattle.game.controller.MainMenuController;
import com.wordbattle.game.network.FirebaseInterface;

public class JoinGameState extends BaseState {

    private JoinGameController controller;
    private FirebaseInterface _FBIC;
    private String errorMsg;

    public JoinGameState(StateManager gsm, FirebaseInterface _FBIC, String errorMsg) {
        super(gsm);
        this._FBIC = _FBIC;
        this.errorMsg = errorMsg;
        this.controller = new JoinGameController(this, _FBIC, errorMsg); // 'this' provides context
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {
    }

    public StateManager getStateManager() {
        return gsm;
    }

    @Override
    public void exit() {
    }

    @Override
    public void dispose() {
    }
}
