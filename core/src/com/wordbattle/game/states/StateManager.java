package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.security.SecureRandom;

public class StateManager {
    private static StateManager instance;
    private GameState currentState;
    private GameState previousState;

    public StateManager() {

    }

    public static StateManager getInstance() {
        if(instance == null){
            instance = new StateManager();
        }
        return instance;
    }

    public void setState(GameState newState) {
        if (currentState != null) {
            previousState = currentState;
            currentState.exit();
        }
        currentState = newState;
        newState.enter();
    }

    public void returnToPreviousState() {
        if (previousState != null) {
            setState(previousState);
        }
    }


    public void update(float dt) {
        if (currentState != null) currentState.update(dt);
    }

    public void render(SpriteBatch batch) {
        if (currentState != null) currentState.render(batch); // Modify this line
    }

    public void dispose() {
    }
}
