package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.CreateGameController;
import com.wordbattle.game.controller.MainMenuController;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.network.FirebaseManager;

public class CreateGameState extends BaseState {
    private CreateGameController controller;
    private FirebaseInterface _FBIC;

    public CreateGameState(StateManager gsm, FirebaseInterface _FBIC) {
        super(gsm);
        this._FBIC = _FBIC;
        this.controller = new CreateGameController(this, _FBIC);
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {
        controller.dispose();
    }

    public StateManager getStateManager() {
        return gsm;
    }
}
