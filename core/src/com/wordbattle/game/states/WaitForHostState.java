package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.WaitForHostController;
import com.wordbattle.game.network.FirebaseInterface;

public class WaitForHostState extends BaseState {

    private WaitForHostController controller;
    private FirebaseInterface _FBIC;
    private String nickname;
    private String pin;

    private int numRounds;




    // Whenever we need to look up the DB or write to DB, you need to pass in whatever
    // data is used with the DB, for instance, here we pass in nickname and gamepin to check
    // whether the pin is a valid pin, and then pass in the players nicknam
    public WaitForHostState(StateManager gsm, FirebaseInterface _FBIC, String nickname, String pin, int numRounds) {
        super(gsm);
        this.nickname = nickname;
        this.pin = pin;
        this._FBIC = _FBIC;
        this.numRounds=numRounds;
        this.controller = new WaitForHostController(this, _FBIC, nickname, pin, numRounds); // 'this' provides context
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }


    @Override
    public void handleInput() {

    }

    public StateManager getStateManager() {
        return gsm;
    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {
        controller.dispose();
    }
}
