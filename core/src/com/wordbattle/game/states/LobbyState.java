package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.LobbyController;
import com.wordbattle.game.network.FirebaseInterface;
import com.wordbattle.game.network.FirebaseManager;

public class LobbyState extends BaseState {
    private LobbyController controller;
    private FirebaseInterface _FBIC;

    public LobbyState(StateManager gsm, String pin, String nickname, String numRounds, FirebaseInterface _FBIC) {
        super(gsm);
        this._FBIC = _FBIC;
        controller = new LobbyController(this, nickname,pin ,numRounds, _FBIC);
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {
    }


    @Override
    public void dispose() {
        controller.dispose(); // eller lobbyView
    }

    public StateManager getStateManager() {
        return gsm;
    }
}
