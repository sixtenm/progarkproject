package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.MainMenuController;
import com.wordbattle.game.controller.WaitingLobbyController;
import com.wordbattle.game.network.FirebaseInterface;

public class WaitingLobbyState extends BaseState {
    private String nickname;
    private int score;
    private FirebaseInterface _FBIC;
    private String pin;

    WaitingLobbyController controller;
    public WaitingLobbyState(StateManager gsm, FirebaseInterface _FBIC, String nickname, int score, String pin,int numRounds) {
        super(gsm);
        this._FBIC = _FBIC;
        this.nickname = nickname;
        this.score = score;
        this.pin = pin;
        this.controller = new WaitingLobbyController(this, _FBIC, nickname, score, pin, numRounds); // 'this' provides context
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);

    }
    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);

    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);

    }

    public StateManager getStateManager(){
        return gsm;
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {
        controller.dispose();

    }
}
