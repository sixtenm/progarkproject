package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.WordBattle;
import com.wordbattle.game.controller.HowToPlayController;
import com.wordbattle.game.controller.SettingsController;
import com.wordbattle.game.network.FirebaseInterface;

public class SettingsState extends BaseState{
    private SettingsController controller;
    private FirebaseInterface _FBIC;


    public SettingsState(StateManager gsm, FirebaseInterface _FBIC){
        super(gsm);
        this._FBIC = _FBIC;
        this.controller = new SettingsController(this, _FBIC);
        cam.setToOrtho(false, WordBattle.WIDTH, WordBattle.HEIGHT);
    }

    public StateManager getStateManager() {
        return gsm;
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void dispose() {

    }
}