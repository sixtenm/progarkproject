package com.wordbattle.game.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordbattle.game.controller.CreateGameController;
import com.wordbattle.game.controller.GamePlayController;
import com.wordbattle.game.network.FirebaseInterface;

public class GamePlayState extends BaseState {

    private GamePlayController controller;
    private FirebaseInterface _FBIC;
    private String pin;
    private String nickname;

    public GamePlayState(StateManager gsm, FirebaseInterface _FBIC, String pin, String nickname, int numRounds) {
        super(gsm);
        this._FBIC = _FBIC;
        this.pin = pin;
        this.controller = new GamePlayController(this, _FBIC, pin, nickname,numRounds);
        this.nickname = nickname;
    }

    public GamePlayController getController() {
        return this.controller;
    }

    @Override
    public void handleInput() {
    }

    @Override
    public void update(float dt) {
        controller.update(dt);
        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        controller.render(sb);
    }

    @Override
    public void enter() {
    }

    @Override
    public void exit() {
    }

    @Override
    public void dispose() {
        controller.dispose();
    }

    public StateManager getStateManager() {
        return gsm;
    }
}
